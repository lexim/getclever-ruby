# Getclever::Ruby

This is a ruby library for interacting with v1.1 of the [Clever](https://getclever.com) API.

## Installation

Add this line to your application's Gemfile:

    gem 'getclever-ruby'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install getclever-ruby

## Configuration

    $ rails g clever:install --api-key your_key_here

The generator adds two files for your rails project:

* config/clever.yml
* config/initializers/clever.rb

Customise the config/clever.yml config file if you wish to use different API tokens per environment (by default it uses the same token in development, test and production).

### Rails application

The Clever API is configured during rails initialization, and no further configuration is necessary before use.

### Non-rails application

When using the Clever gem in a non-rails application, you must configure it to use your Clever API Key manually:

```ruby
Clever.configure do |config|
    config.api_key = 'MY_CLEVER_KEY'
end
```

## Usage

Once configured, you can start using the API:

### Get a list of Districts

```ruby
require 'clever/api'

api = Clever::API.new
districts = api.districts({:limit => 10})

# => {:paging=>nil, :data=>[#<Clever::Models::District:0x007fca7a4fd7f0 @name="Demo District", @id="4fd43cc56d11340000000005">]}

api.districts({:limit => 10})[:data][0].name

# => "Demo District"
```

View the

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
