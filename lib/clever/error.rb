class Clever::Error < StandardError
  attr_accessor :code, :message

  def initialize code, message
    @code = code
    @message = message
  end

  def message
    "HTTP Error #{@code}: #{@message}"
  end
end