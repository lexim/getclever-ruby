class Clever::Models::District
  attr_accessor :id, :name

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end
