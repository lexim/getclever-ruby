class Clever::Models::Photo
  attr_accessor :id, :data

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end