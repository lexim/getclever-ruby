class Clever::Models::Contact
  attr_accessor :id, :email, :name, :phone, :phone_type, :type, :student_id

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end
