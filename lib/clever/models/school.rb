class Clever::Models::School
  attr_accessor :id, :high_grade, :low_grade, :name, :nces_id, :phone, :school_number, :sis_id, :location, :principal, :last_modified

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end
