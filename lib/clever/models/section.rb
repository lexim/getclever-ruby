class Clever::Models::Section
  attr_accessor :id, :grade, :name, :school_id, :sis_id, :section_number, :subject, :students, :teacher_id, :school_id, :district_id, :last_modified

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end
