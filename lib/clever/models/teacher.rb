class Clever::Models::Teacher
  attr_accessor :id, :email, :name, :title, :sis_id, :last_modified, :school_id, :district_id

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end