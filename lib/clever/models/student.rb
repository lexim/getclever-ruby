class Clever::Models::Student
  attr_accessor :id, :name, :dob, :frl_status, :gender, :grade, :hispanic_ethnicity, :race, :school_id, :sis_id, :state_id, :student_number, :location, :name, :district_id, :last_modified

  def initialize params
    params.each { |k,v| instance_variable_set("@#{k}", v) unless v.nil? }
  end
end
