require 'clever/fake_response'

class Clever::FakeServer
  attr_accessor :api_key

  def initialize opts
    @api_key = opts[:api_key]
  end

  def request(req, body=nil)
    return Clever::FakeResponse.new("404", "") unless req.path.start_with? "/v1.1/"

    path, params = req.path.split('?')
    resource, id, action = path.gsub(/(\/v1\.1\/|\?$)/, "").split('/')

    if action.nil? && id.nil?
      path = resource
    elsif id.nil?
      path = resource.gsub(/s$/, '')
    else
      path = resource.gsub(/s$/, '') + (action.nil? ? '' : "_#{action}")
    end

    self.send(path, req.method)

    #rescue => exception
    #  return Clever::FakeResponse.new("500", exception.message)
  end

  def method_missing(sym, *args, &block)
    if Clever::FakeJson.const_defined? sym.to_s.upcase
      return get_json(args[0], Clever::FakeJson.const_get(sym.to_s.upcase))
    end

    super
  end

  def get_json verb, json
    unless verb == "GET"
      return Clever::FakeResponse.new("405", "")
    end

    Clever::FakeResponse.new("200", json)
  end
end

class Clever::FakeJson
  DISTRICTS = '{
                  "paging": {
                    "current": 1,
                    "total": 1,
                    "count": 1
                  },
                  "data": [
                    {
                      "data": {
                        "name": "Demo District",
                        "id": "4fd43cc56d11340000000005"
                      },
                      "uri": "/v1.1/districts/4fd43cc56d11340000000005"
                    }
                  ],
                  "links": [
                    {
                      "rel": "self",
                      "uri": "/v1.1/districts"
                    }
                  ]
                }'

  DISTRICT = '{
                "data": {
                  "name": "Demo District",
                  "id": "4fd43cc56d11340000000005"
                },
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/districts/503675a3da2a1abb43000049"
                  },
                  {
                    "rel": "schools",
                    "uri": "/v1.1/districts/503675a3da2a1abb43000049/schools"
                  },
                  {
                    "rel": "teachers",
                    "uri": "/v1.1/districts/503675a3da2a1abb43000049/teachers"
                  },
                  {
                    "rel": "students",
                    "uri": "/v1.1/districts/503675a3da2a1abb43000049/students"
                  },
                  {
                    "rel": "sections",
                    "uri": "/v1.1/districts/503675a3da2a1abb43000049/sections"
                  }
                ]
              }'

  DISTRICT_SCHOOLS = '{
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "high_grade": "8",
                              "low_grade": "6",
                              "name": "Clever Academy",
                              "nces_id": "17065379",
                              "phone": "1-755-019-5442",
                              "school_number": "214",
                              "sis_id": "9255",
                              "location": {
                                "address": "18828 Kutch Court",
                                "city": "Dessiemouth",
                                "state": "IA",
                                "zip": "37471-9969"
                              },
                              "principal": {
                                "name": "Colleen Gottlieb",
                                "email": "Eda_Barrows@mailinator.com"
                              },
                              "last_modified": "2012-06-29T19:21:50.592Z",
                              "id": "4fee004cca2e43cf27000001"
                            },
                            "uri": "/v1.1/schools/4fee004cca2e43cf27000001"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "high_grade": "8",
                              "low_grade": "6",
                              "name": "Clever Preparatory",
                              "nces_id": "94755881",
                              "phone": "772-761-5171 x26385",
                              "school_number": "426",
                              "sis_id": "2559",
                              "location": {
                                "address": "42139 Fadel Mountains",
                                "city": "Thomasfort",
                                "state": "AP",
                                "zip": "61397-3760"
                              },
                              "principal": {
                                "name": "Rudolph Howe",
                                "email": "Nikolas@mailinator.com"
                              },
                              "last_modified": "2012-06-29T19:21:50.637Z",
                              "id": "4fee004cca2e43cf27000002"
                            },
                            "uri": "/v1.1/schools/4fee004cca2e43cf27000002"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "high_grade": "8",
                              "low_grade": "6",
                              "name": "Clever High School",
                              "nces_id": "44270647",
                              "phone": "1-895-295-3507",
                              "school_number": "481",
                              "sis_id": "4083",
                              "location": {
                                "address": "9538 Leffler Forks",
                                "city": "Gwendolynville",
                                "state": "NH",
                                "zip": "58530-7427"
                              },
                              "principal": {
                                "name": "Krystal Kiehn",
                                "email": "Domingo_Williamson@mailinator.com"
                              },
                              "last_modified": "2012-06-29T19:21:50.661Z",
                              "id": "4fee004cca2e43cf27000003"
                            },
                            "uri": "/v1.1/schools/4fee004cca2e43cf27000003"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "high_grade": "8",
                              "low_grade": "6",
                              "name": "Clever Memorial High",
                              "nces_id": "26309489",
                              "phone": "1-124-215-8079",
                              "school_number": "797",
                              "sis_id": "3935",
                              "location": {
                                "address": "10960 Kilback View",
                                "city": "North Sarina",
                                "state": "MP",
                                "zip": "58717-8256"
                              },
                              "principal": {
                                "name": "Maryse Tillman",
                                "email": "Otto_Sanford@mailinator.com"
                              },
                              "last_modified": "2012-06-29T19:21:50.685Z",
                              "id": "4fee004cca2e43cf27000004"
                            },
                            "uri": "/v1.1/schools/4fee004cca2e43cf27000004"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/districts/4fd43cc56d11340000000005/schools"
                          }
                        ]
                      }'

  DISTRICT_TEACHERS = '{
                          "paging": {
                            "current": 1,
                            "total": 5,
                            "count": 50
                          },
                          "data": [
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "ashlee@mailinator.com",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "1720",
                                "title": "Orchestrate Visionary Technologies Manager",
                                "name": {
                                  "first": "Nadia",
                                  "middle": "H",
                                  "last": "Mitchell"
                                },
                                "last_modified": "2012-06-29T19:22:16.552Z",
                                "id": "4fee004dca2e43cf270007d5"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007d5"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "rowland@mailinator.com",
                                "school": "4fee004cca2e43cf27000002",
                                "sis_id": "6636",
                                "title": "Matrix B2c Markets Manager",
                                "name": {
                                  "first": "Eugenia",
                                  "middle": "L",
                                  "last": "Schuppe"
                                },
                                "last_modified": "2012-06-29T19:22:16.574Z",
                                "id": "4fee004dca2e43cf270007d6"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007d6"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "wilfrid.bogan@mailinator.com",
                                "school": "4fee004cca2e43cf27000004",
                                "sis_id": "4572",
                                "title": "Generate Cross-platform Models Manager",
                                "name": {
                                  "first": "Anibal",
                                  "middle": "N",
                                  "last": "Will"
                                },
                                "last_modified": "2012-06-29T19:22:16.597Z",
                                "id": "4fee004dca2e43cf270007d7"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007d7"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "adaline_kunze@mailinator.com",
                                "school": "4fee004cca2e43cf27000001",
                                "sis_id": "7408",
                                "title": "Disintermediate Visionary Architectures Manager",
                                "name": {
                                  "first": "Alec",
                                  "middle": "G",
                                  "last": "Lubowitz"
                                },
                                "last_modified": "2012-06-29T19:22:16.621Z",
                                "id": "4fee004dca2e43cf270007d8"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007d8"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "lillian@mailinator.com",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "5320",
                                "title": "Harness Ubiquitous Convergence Manager",
                                "name": {
                                  "first": "Bridget",
                                  "middle": "B",
                                  "last": "Kulas"
                                },
                                "last_modified": "2012-06-29T19:22:16.645Z",
                                "id": "4fee004dca2e43cf270007d9"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007d9"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "yessenia@mailinator.com",
                                "school": "4fee004cca2e43cf27000002",
                                "sis_id": "2819",
                                "title": "Drive B2b Bandwidth Manager",
                                "name": {
                                  "first": "Elenor",
                                  "middle": "E",
                                  "last": "Waters"
                                },
                                "last_modified": "2012-06-29T19:22:16.672Z",
                                "id": "4fee004dca2e43cf270007da"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007da"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "grant@mailinator.com",
                                "school": "4fee004cca2e43cf27000001",
                                "sis_id": "7788",
                                "title": "Synergize E-business Technologies Manager",
                                "name": {
                                  "first": "Reta",
                                  "middle": "O",
                                  "last": "West"
                                },
                                "last_modified": "2012-06-29T19:22:16.696Z",
                                "id": "4fee004dca2e43cf270007db"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007db"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "heath@mailinator.com",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "1838",
                                "title": "Grow User-centric Markets Manager",
                                "name": {
                                  "first": "Fannie",
                                  "middle": "F",
                                  "last": "Prosacco"
                                },
                                "last_modified": "2012-06-29T19:22:16.723Z",
                                "id": "4fee004dca2e43cf270007dc"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007dc"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "korbin@mailinator.com",
                                "school": "4fee004cca2e43cf27000004",
                                "sis_id": "4353",
                                "title": "Utilize Customized Channels Manager",
                                "name": {
                                  "first": "Emelie",
                                  "middle": "J",
                                  "last": "Wunsch"
                                },
                                "last_modified": "2012-06-29T19:22:16.749Z",
                                "id": "4fee004dca2e43cf270007dd"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007dd"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "email": "wava.maggio@mailinator.com",
                                "school": "4fee004cca2e43cf27000004",
                                "sis_id": "6628",
                                "title": "Leverage 24/365 Experiences Manager",
                                "name": {
                                  "first": "Nasir",
                                  "middle": "",
                                  "last": "Breitenberg"
                                },
                                "last_modified": "2012-06-29T19:22:16.772Z",
                                "id": "4fee004dca2e43cf270007de"
                              },
                              "uri": "/v1.1/teachers/4fee004dca2e43cf270007de"
                            }
                          ],
                          "links": [
                            {
                              "rel": "self",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/teachers?limit=10"
                            },
                            {
                              "rel": "next",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/teachers?limit=10&page=2"
                            }
                          ]
                        }'

  DISTRICT_STUDENTS = '{
                          "paging": {
                            "current": 1,
                            "total": 200,
                            "count": 1000
                          },
                          "data": [
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "dob": "12/12/1998",
                                "frl_status": "Y",
                                "gender": "M",
                                "grade": "7",
                                "hispanic_ethnicity": "N",
                                "race": "Asian",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "1783",
                                "state_id": "2237504",
                                "student_number": "24772",
                                "location": {
                                  "address": "9272 Ratke Haven",
                                  "city": "North Maurinetown",
                                  "state": "AP",
                                  "zip": "22505-3577",
                                  "lat": "-21.901449509896338",
                                  "lon": "114.23126022331417"
                                },
                                "name": {
                                  "first": "Constance",
                                  "middle": "",
                                  "last": "Roob"
                                },
                                "last_modified": "2012-06-29T19:21:50.709Z",
                                "id": "4fee004cca2e43cf27000005"
                              },
                              "uri": "/v1.1/students/4fee004cca2e43cf27000005"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "dob": "1/18/2000",
                                "frl_status": "Y",
                                "gender": "M",
                                "grade": "8",
                                "hispanic_ethnicity": "N",
                                "race": "Two or More Races",
                                "school": "4fee004cca2e43cf27000002",
                                "sis_id": "2113",
                                "state_id": "4512383",
                                "student_number": "34989",
                                "location": {
                                  "address": "18358 Farrell Corners",
                                  "city": "West Mario",
                                  "state": "AL",
                                  "zip": "42169",
                                  "lat": "20.2340585924685",
                                  "lon": "179.6191607695073"
                                },
                                "name": {
                                  "first": "Roxane",
                                  "middle": "M",
                                  "last": "Weissnat"
                                },
                                "last_modified": "2012-06-29T19:21:50.736Z",
                                "id": "4fee004cca2e43cf27000007"
                              },
                              "uri": "/v1.1/students/4fee004cca2e43cf27000007"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "dob": "10/17/1998",
                                "frl_status": "Y",
                                "gender": "F",
                                "grade": "6",
                                "hispanic_ethnicity": "N",
                                "race": "Caucasian",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "1586",
                                "state_id": "2387048",
                                "student_number": "48387",
                                "location": {
                                  "address": "85702 Cartwright Knoll",
                                  "city": "Port Kasandraport",
                                  "state": "ME",
                                  "zip": "19541-8961",
                                  "lat": "-19.12011313252151",
                                  "lon": "-77.21223609521985"
                                },
                                "name": {
                                  "first": "Lola",
                                  "middle": "S",
                                  "last": "Tremblay"
                                },
                                "last_modified": "2012-06-29T19:21:50.760Z",
                                "id": "4fee004cca2e43cf27000009"
                              },
                              "uri": "/v1.1/students/4fee004cca2e43cf27000009"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "dob": "4/5/1998",
                                "frl_status": "Y",
                                "gender": "F",
                                "grade": "7",
                                "hispanic_ethnicity": "N",
                                "race": "Caucasian",
                                "school": "4fee004cca2e43cf27000003",
                                "sis_id": "4875",
                                "state_id": "3905945",
                                "student_number": "15593",
                                "location": {
                                  "address": "13961 Lueilwitz Cliff",
                                  "city": "Deondreburgh",
                                  "state": "MN",
                                  "zip": "89575-9808",
                                  "lat": "14.23999706748873",
                                  "lon": "128.96796497516334"
                                },
                                "name": {
                                  "first": "Korey",
                                  "middle": "R",
                                  "last": "Orn"
                                },
                                "last_modified": "2012-06-29T19:21:50.785Z",
                                "id": "4fee004cca2e43cf2700000b"
                              },
                              "uri": "/v1.1/students/4fee004cca2e43cf2700000b"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "dob": "10/18/1998",
                                "frl_status": "N",
                                "gender": "F",
                                "grade": "8",
                                "hispanic_ethnicity": "N",
                                "race": "Caucasian",
                                "school": "4fee004cca2e43cf27000004",
                                "sis_id": "7921",
                                "state_id": "3432723",
                                "student_number": "49789",
                                "location": {
                                  "address": "9629 Champlin Loop",
                                  "city": "New Bradley",
                                  "state": "MS",
                                  "zip": "72776",
                                  "lat": "-62.99142876639962",
                                  "lon": "93.68024323135614"
                                },
                                "name": {
                                  "first": "Horace",
                                  "middle": "P",
                                  "last": "Hermann"
                                },
                                "last_modified": "2012-06-29T19:21:50.809Z",
                                "id": "4fee004cca2e43cf2700000d"
                              },
                              "uri": "/v1.1/students/4fee004cca2e43cf2700000d"
                            }
                          ],
                          "links": [
                            {
                              "rel": "self",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/students?limit=5"
                            },
                            {
                              "rel": "next",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/students?limit=5&page=2"
                            }
                          ]
                        }'

  DISTRICT_SECTIONS = '{
                          "paging": {
                            "current": 1,
                            "total": 37,
                            "count": 73
                          },
                          "data": [
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "grade": "7",
                                "name": "Advanced Arithmetic 8(A)",
                                "school": "4fee004cca2e43cf27000004",
                                "section_number": "76",
                                "sis_id": "3838",
                                "subject": "math",
                                "teacher": "4fee004dca2e43cf270007f4",
                                "students": [
                                  "4fee004dca2e43cf270004ed",
                                  "4fee004dca2e43cf2700069f",
                                  "4fee004cca2e43cf27000177",
                                  "4fee004cca2e43cf270002b7",
                                  "4fee004cca2e43cf27000437",
                                  "4fee004dca2e43cf2700066b",
                                  "4fee004dca2e43cf2700061f",
                                  "4fee004dca2e43cf27000701",
                                  "4fee004dca2e43cf2700054d",
                                  "4fee004cca2e43cf270002b5",
                                  "4fee004dca2e43cf27000501",
                                  "4fee004dca2e43cf27000617",
                                  "4fee004cca2e43cf270000af",
                                  "4fee004dca2e43cf27000597",
                                  "4fee004dca2e43cf27000719",
                                  "4fee004cca2e43cf27000043",
                                  "4fee004dca2e43cf27000755",
                                  "4fee004cca2e43cf270003df",
                                  "4fee004cca2e43cf27000221",
                                  "4fee004dca2e43cf2700054b",
                                  "4fee004dca2e43cf270004ff",
                                  "4fee004cca2e43cf27000325"
                                ],
                                "last_modified": "2012-06-29T19:22:17.766Z",
                                "id": "4fee004dca2e43cf27000807"
                              },
                              "uri": "/v1.1/sections/4fee004dca2e43cf27000807"
                            },
                            {
                              "data": {
                                "district": "4fd43cc56d11340000000005",
                                "grade": "6",
                                "name": "Planetary Chemistry 5(B)",
                                "school": "4fee004cca2e43cf27000001",
                                "section_number": "85",
                                "sis_id": "4917",
                                "subject": "science",
                                "teacher": "4fee004dca2e43cf270007f9",
                                "students": [
                                  "4fee004cca2e43cf2700008b",
                                  "4fee004dca2e43cf270006f9",
                                  "4fee004cca2e43cf27000201",
                                  "4fee004dca2e43cf27000675",
                                  "4fee004cca2e43cf2700011f",
                                  "4fee004cca2e43cf2700016f",
                                  "4fee004dca2e43cf2700076d",
                                  "4fee004dca2e43cf27000777",
                                  "4fee004cca2e43cf270001b7",
                                  "4fee004cca2e43cf270002c9",
                                  "4fee004dca2e43cf270005a9",
                                  "4fee004cca2e43cf2700030d",
                                  "4fee004cca2e43cf2700003f",
                                  "4fee004dca2e43cf2700076d",
                                  "4fee004dca2e43cf27000769",
                                  "4fee004cca2e43cf2700042b",
                                  "4fee004dca2e43cf27000551",
                                  "4fee004cca2e43cf27000071",
                                  "4fee004dca2e43cf270005cd",
                                  "4fee004dca2e43cf2700045f",
                                  "4fee004dca2e43cf270004bf",
                                  "4fee004cca2e43cf27000017",
                                  "4fee004dca2e43cf270004f9",
                                  "4fee004cca2e43cf27000417",
                                  "4fee004cca2e43cf27000213",
                                  "4fee004cca2e43cf2700013b",
                                  "4fee004dca2e43cf27000699",
                                  "4fee004dca2e43cf27000787",
                                  "4fee004cca2e43cf27000049",
                                  "4fee004dca2e43cf270005e5",
                                  "4fee004dca2e43cf2700052d",
                                  "4fee004dca2e43cf27000479",
                                  "4fee004cca2e43cf2700010f",
                                  "4fee004dca2e43cf270004af",
                                  "4fee004cca2e43cf27000425"
                                ],
                                "last_modified": "2012-06-29T19:22:17.789Z",
                                "id": "4fee004dca2e43cf27000808"
                              },
                              "uri": "/v1.1/sections/4fee004dca2e43cf27000808"
                            }
                          ],
                          "links": [
                            {
                              "rel": "self",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/sections?limit=2"
                            },
                            {
                              "rel": "next",
                              "uri": "/v1.1/districts/4fd43cc56d11340000000005/sections?limit=2&page=2"
                            }
                          ]
                        }'

  SCHOOLS = '{
                "paging": {
                  "current": 1,
                  "total": 2,
                  "count": 4
                },
                "data": [
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "high_grade": "8",
                      "low_grade": "6",
                      "name": "Clever Academy",
                      "nces_id": "17065379",
                      "phone": "1-755-019-5442",
                      "school_number": "214",
                      "sis_id": "9255",
                      "location": {
                        "address": "18828 Kutch Court",
                        "city": "Dessiemouth",
                        "state": "IA",
                        "zip": "37471-9969"
                      },
                      "principal": {
                        "name": "Colleen Gottlieb",
                        "email": "Eda_Barrows@mailinator.com"
                      },
                      "last_modified": "2012-06-29T19:21:50.592Z",
                      "id": "4fee004cca2e43cf27000001"
                    },
                    "uri": "/v1.1/schools/4fee004cca2e43cf27000001"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "high_grade": "8",
                      "low_grade": "6",
                      "name": "Clever Preparatory",
                      "nces_id": "94755881",
                      "phone": "772-761-5171 x26385",
                      "school_number": "426",
                      "sis_id": "2559",
                      "location": {
                        "address": "42139 Fadel Mountains",
                        "city": "Thomasfort",
                        "state": "AP",
                        "zip": "61397-3760"
                      },
                      "principal": {
                        "name": "Rudolph Howe",
                        "email": "Nikolas@mailinator.com"
                      },
                      "last_modified": "2012-06-29T19:21:50.637Z",
                      "id": "4fee004cca2e43cf27000002"
                    },
                    "uri": "/v1.1/schools/4fee004cca2e43cf27000002"
                  }
                ],
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/schools?limit=2"
                  },
                  {
                    "rel": "next",
                    "uri": "/v1.1/schools?limit=2&page=2"
                  }
                ]
              }'

  SCHOOL = '{
              "data": {
                "district": "4fd43cc56d11340000000005",
                "high_grade": "8",
                "low_grade": "6",
                "name": "Clever Academy",
                "nces_id": "17065379",
                "phone": "1-755-019-5442",
                "school_number": "214",
                "sis_id": "9255",
                "location": {
                  "address": "18828 Kutch Court",
                  "city": "Dessiemouth",
                  "state": "IA",
                  "zip": "37471-9969"
                },
                "principal": {
                  "name": "Colleen Gottlieb",
                  "email": "Eda_Barrows@mailinator.com"
                },
                "last_modified": "2012-06-29T19:21:50.592Z",
                "id": "4fee004cca2e43cf27000001"
              },
              "links": [
                {
                  "rel": "self",
                  "uri": "/v1.1/schools/4fd43f4a5e5e397767000005"
                },
                {
                  "rel": "district",
                  "uri": "/v1.1/schools/4fd43f4a5e5e397767000005/district"
                },
                {
                  "rel": "teachers",
                  "uri": "/v1.1/schools/4fd43f4a5e5e397767000005/teachers"
                },
                {
                  "rel": "students",
                  "uri": "/v1.1/schools/4fd43f4a5e5e397767000005/students"
                },
                {
                  "rel": "sections",
                  "uri": "/v1.1/schools/4fd43f4a5e5e397767000005/sections"
                }
              ]
            }'

  SCHOOL_TEACHERS = '{
                      "paging": {
                        "current": 1,
                        "total": 2,
                        "count": 12
                      },
                      "data": [
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "adaline_kunze@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "7408",
                            "title": "Disintermediate Visionary Architectures Manager",
                            "name": {
                              "first": "Alec",
                              "middle": "G",
                              "last": "Lubowitz"
                            },
                            "last_modified": "2012-06-29T19:22:16.621Z",
                            "id": "4fee004dca2e43cf270007d8"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007d8"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "grant@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "7788",
                            "title": "Synergize E-business Technologies Manager",
                            "name": {
                              "first": "Reta",
                              "middle": "O",
                              "last": "West"
                            },
                            "last_modified": "2012-06-29T19:22:16.696Z",
                            "id": "4fee004dca2e43cf270007db"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007db"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "toby_stracke@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "2464",
                            "title": "E-enable Virtual Portals Manager",
                            "name": {
                              "first": "Baron",
                              "middle": "J",
                              "last": "Shields"
                            },
                            "last_modified": "2012-06-29T19:22:16.868Z",
                            "id": "4fee004dca2e43cf270007e2"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007e2"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "woodrow_hessel@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "2710",
                            "title": "Redefine Virtual Solutions Manager",
                            "name": {
                              "first": "Curt",
                              "middle": "H",
                              "last": "Dietrich"
                            },
                            "last_modified": "2012-06-29T19:22:17.108Z",
                            "id": "4fee004dca2e43cf270007ec"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007ec"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "alessandra@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "9417",
                            "title": "Extend Sexy Web-readiness Manager",
                            "name": {
                              "first": "Brendan",
                              "middle": "D",
                              "last": "Fay"
                            },
                            "last_modified": "2012-06-29T19:22:17.158Z",
                            "id": "4fee004dca2e43cf270007ee"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007ee"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "stephany@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "2529",
                            "title": "Benchmark Value-added Communities Manager",
                            "name": {
                              "first": "Letitia",
                              "middle": "S",
                              "last": "Padberg"
                            },
                            "last_modified": "2012-06-29T19:22:17.278Z",
                            "id": "4fee004dca2e43cf270007f3"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007f3"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "gordon@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "419",
                            "title": "Grow Next-generation Networks Manager",
                            "name": {
                              "first": "Samanta",
                              "middle": "B",
                              "last": "Hagenes"
                            },
                            "last_modified": "2012-06-29T19:22:17.349Z",
                            "id": "4fee004dca2e43cf270007f6"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007f6"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "aida@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "7839",
                            "title": "Revolutionize Robust Interfaces Manager",
                            "name": {
                              "first": "Kaley",
                              "middle": "S",
                              "last": "McKenzie"
                            },
                            "last_modified": "2012-06-29T19:22:17.423Z",
                            "id": "4fee004dca2e43cf270007f9"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007f9"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "juston@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "8518",
                            "title": "Leverage Magnetic Roi Manager",
                            "name": {
                              "first": "Chadrick",
                              "middle": "M",
                              "last": "Pfeffer"
                            },
                            "last_modified": "2012-06-29T19:22:17.446Z",
                            "id": "4fee004dca2e43cf270007fa"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007fa"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "email": "aiyana.adams@mailinator.com",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "6173",
                            "title": "Transform Bleeding-edge Roi Manager",
                            "name": {
                              "first": "Dillan",
                              "middle": "Q",
                              "last": "Haag"
                            },
                            "last_modified": "2012-06-29T19:22:17.668Z",
                            "id": "4fee004dca2e43cf27000803"
                          },
                          "uri": "/v1.1/teachers/4fee004dca2e43cf27000803"
                        }
                      ],
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/teachers?limit=10"
                        },
                        {
                          "rel": "next",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/teachers?limit=10&page=2"
                        }
                      ]
                    }'

  SCHOOL_STUDENTS = '{
                      "paging": {
                        "current": 1,
                        "total": 60,
                        "count": 239
                      },
                      "data": [
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "dob": "1/18/2000",
                            "frl_status": "N",
                            "gender": "M",
                            "grade": "8",
                            "hispanic_ethnicity": "N",
                            "race": "Black or African American",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "2538",
                            "state_id": "1673570",
                            "student_number": "47484",
                            "location": {
                              "address": "52572 Yost Ports",
                              "city": "Krystinaburgh",
                              "state": "MO",
                              "zip": "52063-3438",
                              "lat": "-43.32432461902499",
                              "lon": "35.24701637215912"
                            },
                            "name": {
                              "first": "Jacinthe",
                              "middle": "N",
                              "last": "Wunsch"
                            },
                            "last_modified": "2012-06-29T19:21:50.881Z",
                            "id": "4fee004cca2e43cf27000013"
                          },
                          "uri": "/v1.1/students/4fee004cca2e43cf27000013"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "dob": "7/8/1999",
                            "frl_status": "Y",
                            "gender": "M",
                            "grade": "7",
                            "hispanic_ethnicity": "N",
                            "race": "Two or More Races",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "9268",
                            "state_id": "1875659",
                            "student_number": "28368",
                            "location": {
                              "address": "9107 Kohler Island",
                              "city": "East Briana",
                              "state": "MS",
                              "zip": "57792-3889",
                              "lat": "-35.49765639938414",
                              "lon": "-94.93370630778372"
                            },
                            "name": {
                              "first": "Columbus",
                              "middle": "I",
                              "last": "Herman"
                            },
                            "last_modified": "2012-06-29T19:21:50.969Z",
                            "id": "4fee004cca2e43cf27000017"
                          },
                          "uri": "/v1.1/students/4fee004cca2e43cf27000017"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "dob": "10/14/2000",
                            "frl_status": "N",
                            "gender": "M",
                            "grade": "8",
                            "hispanic_ethnicity": "N",
                            "race": "Caucasian",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "2534",
                            "state_id": "2771181",
                            "student_number": "40477",
                            "location": {
                              "address": "0831 Abbott Drives",
                              "city": "South Courtney",
                              "state": "VA",
                              "zip": "93269-6992",
                              "lat": "6.88929773401469",
                              "lon": "149.16023835539818"
                            },
                            "name": {
                              "first": "Jennie",
                              "middle": "D",
                              "last": "Kuvalis"
                            },
                            "last_modified": "2012-06-29T19:21:50.992Z",
                            "id": "4fee004cca2e43cf27000019"
                          },
                          "uri": "/v1.1/students/4fee004cca2e43cf27000019"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "dob": "1/3/1999",
                            "frl_status": "N",
                            "gender": "F",
                            "grade": "6",
                            "hispanic_ethnicity": "N",
                            "race": "Hawaiian or Other Pacific Islander",
                            "school": "4fee004cca2e43cf27000001",
                            "sis_id": "645",
                            "state_id": "3560709",
                            "student_number": "48331",
                            "location": {
                              "address": "2783 Beahan Isle",
                              "city": "East Gunnarside",
                              "state": "FL",
                              "zip": "77603",
                              "lat": "80.54116220679134",
                              "lon": "161.40669501386583"
                            },
                            "name": {
                              "first": "Keshawn",
                              "middle": "D",
                              "last": "Ernser"
                            },
                            "last_modified": "2012-06-29T19:21:51.141Z",
                            "id": "4fee004cca2e43cf27000025"
                          },
                          "uri": "/v1.1/students/4fee004cca2e43cf27000025"
                        }
                      ],
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/students?limit=4"
                        },
                        {
                          "rel": "next",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/students?limit=4&page=2"
                        }
                      ]
                    }'

  SCHOOL_SECTIONS = '{
                      "paging": {
                        "current": 1,
                        "total": 9,
                        "count": 17
                      },
                      "data": [
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "grade": "6",
                            "name": "Planetary Chemistry 5(B)",
                            "school": "4fee004cca2e43cf27000001",
                            "section_number": "85",
                            "sis_id": "4917",
                            "subject": "science",
                            "teacher": "4fee004dca2e43cf270007f9",
                            "students": [
                              "4fee004cca2e43cf2700008b",
                              "4fee004dca2e43cf270006f9",
                              "4fee004cca2e43cf27000201",
                              "4fee004dca2e43cf27000675",
                              "4fee004cca2e43cf2700011f",
                              "4fee004cca2e43cf2700016f",
                              "4fee004dca2e43cf2700076d",
                              "4fee004dca2e43cf27000777",
                              "4fee004cca2e43cf270001b7",
                              "4fee004cca2e43cf270002c9",
                              "4fee004dca2e43cf270005a9",
                              "4fee004cca2e43cf2700030d",
                              "4fee004cca2e43cf2700003f",
                              "4fee004dca2e43cf2700076d",
                              "4fee004dca2e43cf27000769",
                              "4fee004cca2e43cf2700042b",
                              "4fee004dca2e43cf27000551",
                              "4fee004cca2e43cf27000071",
                              "4fee004dca2e43cf270005cd",
                              "4fee004dca2e43cf2700045f",
                              "4fee004dca2e43cf270004bf",
                              "4fee004cca2e43cf27000017",
                              "4fee004dca2e43cf270004f9",
                              "4fee004cca2e43cf27000417",
                              "4fee004cca2e43cf27000213",
                              "4fee004cca2e43cf2700013b",
                              "4fee004dca2e43cf27000699",
                              "4fee004dca2e43cf27000787",
                              "4fee004cca2e43cf27000049",
                              "4fee004dca2e43cf270005e5",
                              "4fee004dca2e43cf2700052d",
                              "4fee004dca2e43cf27000479",
                              "4fee004cca2e43cf2700010f",
                              "4fee004dca2e43cf270004af",
                              "4fee004cca2e43cf27000425"
                            ],
                            "last_modified": "2012-06-29T19:22:17.789Z",
                            "id": "4fee004dca2e43cf27000808"
                          },
                          "uri": "/v1.1/sections/4fee004dca2e43cf27000808"
                        },
                        {
                          "data": {
                            "district": "4fd43cc56d11340000000005",
                            "grade": "6",
                            "name": "Earth Physics 4(B)",
                            "school": "4fee004cca2e43cf27000001",
                            "section_number": "15",
                            "sis_id": "6323",
                            "subject": "science",
                            "teacher": "4fee004dca2e43cf270007d8",
                            "students": [
                              "4fee004cca2e43cf270003eb",
                              "4fee004dca2e43cf270005ef",
                              "4fee004cca2e43cf27000351",
                              "4fee004dca2e43cf270005cd",
                              "4fee004dca2e43cf27000487",
                              "4fee004dca2e43cf270007cf",
                              "4fee004cca2e43cf27000389",
                              "4fee004cca2e43cf27000115",
                              "4fee004cca2e43cf2700007b",
                              "4fee004dca2e43cf270005eb",
                              "4fee004cca2e43cf2700037f",
                              "4fee004cca2e43cf270001a5",
                              "4fee004dca2e43cf2700052b",
                              "4fee004dca2e43cf27000665",
                              "4fee004cca2e43cf270002d1",
                              "4fee004cca2e43cf27000339",
                              "4fee004cca2e43cf27000019",
                              "4fee004cca2e43cf27000161",
                              "4fee004dca2e43cf27000491",
                              "4fee004dca2e43cf270004d5",
                              "4fee004cca2e43cf2700042b",
                              "4fee004cca2e43cf27000025",
                              "4fee004dca2e43cf2700056b",
                              "4fee004dca2e43cf270005cd",
                              "4fee004cca2e43cf270003b7",
                              "4fee004dca2e43cf270006e5",
                              "4fee004cca2e43cf27000273",
                              "4fee004dca2e43cf270006f9",
                              "4fee004dca2e43cf270007d1",
                              "4fee004cca2e43cf2700022f",
                              "4fee004cca2e43cf2700025f",
                              "4fee004cca2e43cf270002eb",
                              "4fee004cca2e43cf27000281",
                              "4fee004cca2e43cf27000289",
                              "4fee004cca2e43cf27000017",
                              "4fee004dca2e43cf27000647"
                            ],
                            "last_modified": "2012-06-29T19:22:17.935Z",
                            "id": "4fee004dca2e43cf2700080e"
                          },
                          "uri": "/v1.1/sections/4fee004dca2e43cf2700080e"
                        }
                      ],
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/sections?limit=2"
                        },
                        {
                          "rel": "next",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000001/sections?limit=2&page=2"
                        }
                      ]
                    }'

  SCHOOL_DISTRICT = '{
                      "data": {
                        "name": "Demo District",
                        "id": "4fd43cc56d11340000000005"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/districts/4fd43cc56d11340000000005"
                        }
                      ]
                    }'

  TEACHERS = '{
                "paging": {
                  "current": 1,
                  "total": 13,
                  "count": 50
                },
                "data": [
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "email": "ashlee@mailinator.com",
                      "school": "4fee004cca2e43cf27000003",
                      "sis_id": "1720",
                      "title": "Orchestrate Visionary Technologies Manager",
                      "name": {
                        "first": "Nadia",
                        "middle": "H",
                        "last": "Mitchell"
                      },
                      "last_modified": "2012-06-29T19:22:16.552Z",
                      "id": "4fee004dca2e43cf270007d5"
                    },
                    "uri": "/v1.1/teachers/4fee004dca2e43cf270007d5"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "email": "rowland@mailinator.com",
                      "school": "4fee004cca2e43cf27000002",
                      "sis_id": "6636",
                      "title": "Matrix B2c Markets Manager",
                      "name": {
                        "first": "Eugenia",
                        "middle": "L",
                        "last": "Schuppe"
                      },
                      "last_modified": "2012-06-29T19:22:16.574Z",
                      "id": "4fee004dca2e43cf270007d6"
                    },
                    "uri": "/v1.1/teachers/4fee004dca2e43cf270007d6"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "email": "wilfrid.bogan@mailinator.com",
                      "school": "4fee004cca2e43cf27000004",
                      "sis_id": "4572",
                      "title": "Generate Cross-platform Models Manager",
                      "name": {
                        "first": "Anibal",
                        "middle": "N",
                        "last": "Will"
                      },
                      "last_modified": "2012-06-29T19:22:16.597Z",
                      "id": "4fee004dca2e43cf270007d7"
                    },
                    "uri": "/v1.1/teachers/4fee004dca2e43cf270007d7"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "email": "adaline_kunze@mailinator.com",
                      "school": "4fee004cca2e43cf27000001",
                      "sis_id": "7408",
                      "title": "Disintermediate Visionary Architectures Manager",
                      "name": {
                        "first": "Alec",
                        "middle": "G",
                        "last": "Lubowitz"
                      },
                      "last_modified": "2012-06-29T19:22:16.621Z",
                      "id": "4fee004dca2e43cf270007d8"
                    },
                    "uri": "/v1.1/teachers/4fee004dca2e43cf270007d8"
                  }
                ],
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/teachers?limit=4"
                  },
                  {
                    "rel": "next",
                    "uri": "/v1.1/teachers?limit=4&page=2"
                  }
                ]
              }'

  TEACHER = '{
              "data": {
                "district": "4fd43cc56d11340000000005",
                "email": "ashlee@mailinator.com",
                "school": "4fee004cca2e43cf27000003",
                "sis_id": "1720",
                "title": "Orchestrate Visionary Technologies Manager",
                "name": {
                  "first": "Nadia",
                  "middle": "H",
                  "last": "Mitchell"
                },
                "last_modified": "2012-06-29T19:22:16.552Z",
                "id": "4fee004dca2e43cf270007d5"
              },
              "links": [
                {
                  "rel": "self",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b"
                },
                {
                  "rel": "district",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b/district"
                },
                {
                  "rel": "school",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b/school"
                },
                {
                  "rel": "students",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b/students"
                },
                {
                  "rel": "sections",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b/sections"
                },
                {
                  "rel": "grade_levels",
                  "uri": "/v1.1/teachers/4ffefa20f608049536000b6b/grade_levels"
                }
              ]
            }'

  TEACHER_SECTIONS = '{
                        "paging": {
                          "current": 1,
                          "total": 2,
                          "count": 3
                        },
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "grade": "7",
                              "name": "Earth Arithmetic 4(B)",
                              "school": "4fee004cca2e43cf27000004",
                              "section_number": "64",
                              "sis_id": "1776",
                              "subject": "math",
                              "teacher": "4fee004dca2e43cf270007fd",
                              "students": [
                                "4fee004dca2e43cf2700047b",
                                "4fee004cca2e43cf27000211",
                                "4fee004cca2e43cf27000271",
                                "4fee004dca2e43cf27000677",
                                "4fee004dca2e43cf2700048d",
                                "4fee004cca2e43cf27000061",
                                "4fee004dca2e43cf270004a9",
                                "4fee004cca2e43cf27000327",
                                "4fee004cca2e43cf2700031d",
                                "4fee004dca2e43cf270004ef",
                                "4fee004dca2e43cf27000601",
                                "4fee004dca2e43cf27000565",
                                "4fee004cca2e43cf2700021b",
                                "4fee004cca2e43cf27000189",
                                "4fee004cca2e43cf27000435",
                                "4fee004cca2e43cf27000085",
                                "4fee004dca2e43cf270004ed",
                                "4fee004dca2e43cf270007cb",
                                "4fee004cca2e43cf270003d1",
                                "4fee004cca2e43cf27000297",
                                "4fee004cca2e43cf27000241",
                                "4fee004cca2e43cf2700009f",
                                "4fee004dca2e43cf270007a5"
                              ],
                              "last_modified": "2012-06-29T19:22:18.231Z",
                              "id": "4fee004dca2e43cf2700081a"
                            },
                            "uri": "/v1.1/sections/4fee004dca2e43cf2700081a"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "grade": "8",
                              "name": "Earth Arithmetic 3(B)",
                              "school": "4fee004cca2e43cf27000004",
                              "section_number": "60",
                              "sis_id": "3095",
                              "subject": "math",
                              "teacher": "4fee004dca2e43cf270007fd",
                              "students": [
                                "4fee004cca2e43cf27000241",
                                "4fee004cca2e43cf27000379",
                                "4fee004dca2e43cf2700071b",
                                "4fee004cca2e43cf27000327",
                                "4fee004cca2e43cf2700011b",
                                "4fee004cca2e43cf2700023f",
                                "4fee004dca2e43cf270004db",
                                "4fee004dca2e43cf2700049b",
                                "4fee004dca2e43cf270005c1",
                                "4fee004cca2e43cf27000211",
                                "4fee004dca2e43cf270005ad",
                                "4fee004cca2e43cf2700030b",
                                "4fee004cca2e43cf27000069",
                                "4fee004dca2e43cf270005cb",
                                "4fee004dca2e43cf27000441",
                                "4fee004cca2e43cf2700019f",
                                "4fee004cca2e43cf270001f7",
                                "4fee004cca2e43cf270001cb",
                                "4fee004dca2e43cf270005d3",
                                "4fee004dca2e43cf2700078b",
                                "4fee004cca2e43cf2700038b",
                                "4fee004cca2e43cf27000065",
                                "4fee004dca2e43cf270004a7",
                                "4fee004dca2e43cf27000511",
                                "4fee004cca2e43cf2700024d",
                                "4fee004cca2e43cf270001e5",
                                "4fee004dca2e43cf27000677",
                                "4fee004dca2e43cf2700052f",
                                "4fee004cca2e43cf270000a3",
                                "4fee004cca2e43cf270000b7",
                                "4fee004dca2e43cf2700069f",
                                "4fee004cca2e43cf2700004b",
                                "4fee004cca2e43cf2700037d",
                                "4fee004cca2e43cf270001f7",
                                "4fee004dca2e43cf27000713",
                                "4fee004dca2e43cf27000483"
                              ],
                              "last_modified": "2012-06-29T19:22:18.922Z",
                              "id": "4fee004dca2e43cf27000835"
                            },
                            "uri": "/v1.1/sections/4fee004dca2e43cf27000835"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007fd/sections?limit=2"
                          },
                          {
                            "rel": "next",
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007fd/sections?limit=2&page=2"
                          }
                        ]
                      }'

  TEACHER_SCHOOL = '{
                      "data": {
                        "district": "4fd43cc56d11340000000005",
                        "high_grade": "8",
                        "low_grade": "6",
                        "name": "Clever High School",
                        "nces_id": "44270647",
                        "phone": "1-895-295-3507",
                        "school_number": "481",
                        "sis_id": "4083",
                        "location": {
                          "address": "9538 Leffler Forks",
                          "city": "Gwendolynville",
                          "state": "NH",
                          "zip": "58530-7427"
                        },
                        "principal": {
                          "name": "Krystal Kiehn",
                          "email": "Domingo_Williamson@mailinator.com"
                        },
                        "last_modified": "2012-06-29T19:21:50.661Z",
                        "id": "4fee004cca2e43cf27000003"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000003"
                        }
                      ]
                    }'

  TEACHER_DISTRICT = '{
                        "data": {
                          "name": "Demo District",
                          "id": "4fd43cc56d11340000000005"
                        },
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/districts/4fd43cc56d11340000000005"
                          }
                        ]
                      }'

  TEACHER_STUDENTS = '{
                        "paging": {
                          "current": 1,
                          "total": 17,
                          "count": 81
                        },
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "10/18/1998",
                              "frl_status": "N",
                              "gender": "F",
                              "grade": "8",
                              "hispanic_ethnicity": "N",
                              "race": "Caucasian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "7921",
                              "state_id": "3432723",
                              "student_number": "49789",
                              "location": {
                                "address": "9629 Champlin Loop",
                                "city": "New Bradley",
                                "state": "MS",
                                "zip": "72776",
                                "lat": "-62.99142876639962",
                                "lon": "93.68024323135614"
                              },
                              "name": {
                                "first": "Horace",
                                "middle": "P",
                                "last": "Hermann"
                              },
                              "last_modified": "2012-06-29T19:21:50.809Z",
                              "id": "4fee004cca2e43cf2700000d"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf2700000d"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "9/28/2000",
                              "frl_status": "N",
                              "gender": "M",
                              "grade": "7",
                              "hispanic_ethnicity": "N",
                              "race": "American Indian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "1122",
                              "state_id": "2139858",
                              "student_number": "22149",
                              "location": {
                                "address": "3613 Heller Forges",
                                "city": "North Sonia",
                                "state": "OR",
                                "zip": "97420",
                                "lat": "70.63689352478832",
                                "lon": "-97.28568367660046"
                              },
                              "name": {
                                "first": "Matilda",
                                "middle": "E",
                                "last": "Bednar"
                              },
                              "last_modified": "2012-06-29T19:21:51.607Z",
                              "id": "4fee004cca2e43cf2700004b"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf2700004b"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "8/8/2000",
                              "frl_status": "Y",
                              "gender": "M",
                              "grade": "7",
                              "hispanic_ethnicity": "N",
                              "race": "Asian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "860",
                              "state_id": "3000099",
                              "student_number": "25151",
                              "location": {
                                "address": "0639 Kilback Well",
                                "city": "West Alvina",
                                "state": "CT",
                                "zip": "77822",
                                "lat": "21.767521952278912",
                                "lon": "-82.98645811155438"
                              },
                              "name": {
                                "first": "Wanda",
                                "middle": "C",
                                "last": "Cruickshank"
                              },
                              "last_modified": "2012-06-29T19:21:51.633Z",
                              "id": "4fee004cca2e43cf2700004d"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf2700004d"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "7/27/1999",
                              "frl_status": "N",
                              "gender": "F",
                              "grade": "8",
                              "hispanic_ethnicity": "Y",
                              "race": "Caucasian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "5300",
                              "state_id": "4116063",
                              "student_number": "35504",
                              "location": {
                                "address": "9563 Rau Causeway",
                                "city": "South Lilliefurt",
                                "state": "PW",
                                "zip": "18942",
                                "lat": "-83.75890916213393",
                                "lon": "34.091629944741726"
                              },
                              "name": {
                                "first": "Brycen",
                                "middle": "Q",
                                "last": "Treutel"
                              },
                              "last_modified": "2012-06-29T19:21:51.887Z",
                              "id": "4fee004cca2e43cf27000061"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf27000061"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "5/4/1998",
                              "frl_status": "N",
                              "gender": "F",
                              "grade": "8",
                              "hispanic_ethnicity": "N",
                              "race": "Asian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "8407",
                              "state_id": "3146996",
                              "student_number": "28368",
                              "location": {
                                "address": "4036 Kub Summit",
                                "city": "New Constantin",
                                "state": "MD",
                                "zip": "61363",
                                "lat": "-80.20724730100483",
                                "lon": "-135.19483733922243"
                              },
                              "name": {
                                "first": "Roberto",
                                "last": "Garcia",
                                "middle": "Y"
                              },
                              "last_modified": "2012-06-29T19:21:51.935Z",
                              "id": "4fee004cca2e43cf27000065"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf27000065"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007fd/students?limit=5"
                          },
                          {
                            "rel": "next",
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007fd/students?limit=5&page=2"
                          }
                        ]
                      }'

  TEACHER_GRADE_LEVELS = '{
                            "data": [
                              "6",
                              "7",
                              "8"
                            ],
                            "links": [
                              {
                                "rel": "self",
                                "uri": "/v1.1/teachers/4fee004dca2e43cf270007fd/grade_levels"
                              }
                            ]
                          }'

  STUDENTS = '{
                "paging": {
                  "current": 1,
                  "total": 200,
                  "count": 1000
                },
                "data": [
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "dob": "12/12/1998",
                      "frl_status": "Y",
                      "gender": "M",
                      "grade": "7",
                      "hispanic_ethnicity": "N",
                      "race": "Asian",
                      "school": "4fee004cca2e43cf27000003",
                      "sis_id": "1783",
                      "state_id": "2237504",
                      "student_number": "24772",
                      "location": {
                        "address": "9272 Ratke Haven",
                        "city": "North Maurinetown",
                        "state": "AP",
                        "zip": "22505-3577",
                        "lat": "-21.901449509896338",
                        "lon": "114.23126022331417"
                      },
                      "name": {
                        "first": "Constance",
                        "middle": "",
                        "last": "Roob"
                      },
                      "last_modified": "2012-06-29T19:21:50.709Z",
                      "id": "4fee004cca2e43cf27000005"
                    },
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "dob": "1/18/2000",
                      "frl_status": "Y",
                      "gender": "M",
                      "grade": "8",
                      "hispanic_ethnicity": "N",
                      "race": "Two or More Races",
                      "school": "4fee004cca2e43cf27000002",
                      "sis_id": "2113",
                      "state_id": "4512383",
                      "student_number": "34989",
                      "location": {
                        "address": "18358 Farrell Corners",
                        "city": "West Mario",
                        "state": "AL",
                        "zip": "42169",
                        "lat": "20.2340585924685",
                        "lon": "179.6191607695073"
                      },
                      "name": {
                        "first": "Roxane",
                        "middle": "M",
                        "last": "Weissnat"
                      },
                      "last_modified": "2012-06-29T19:21:50.736Z",
                      "id": "4fee004cca2e43cf27000007"
                    },
                    "uri": "/v1.1/students/4fee004cca2e43cf27000007"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "dob": "10/17/1998",
                      "frl_status": "Y",
                      "gender": "F",
                      "grade": "6",
                      "hispanic_ethnicity": "N",
                      "race": "Caucasian",
                      "school": "4fee004cca2e43cf27000003",
                      "sis_id": "1586",
                      "state_id": "2387048",
                      "student_number": "48387",
                      "location": {
                        "address": "85702 Cartwright Knoll",
                        "city": "Port Kasandraport",
                        "state": "ME",
                        "zip": "19541-8961",
                        "lat": "-19.12011313252151",
                        "lon": "-77.21223609521985"
                      },
                      "name": {
                        "first": "Lola",
                        "middle": "S",
                        "last": "Tremblay"
                      },
                      "last_modified": "2012-06-29T19:21:50.760Z",
                      "id": "4fee004cca2e43cf27000009"
                    },
                    "uri": "/v1.1/students/4fee004cca2e43cf27000009"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "dob": "4/5/1998",
                      "frl_status": "Y",
                      "gender": "F",
                      "grade": "7",
                      "hispanic_ethnicity": "N",
                      "race": "Caucasian",
                      "school": "4fee004cca2e43cf27000003",
                      "sis_id": "4875",
                      "state_id": "3905945",
                      "student_number": "15593",
                      "location": {
                        "address": "13961 Lueilwitz Cliff",
                        "city": "Deondreburgh",
                        "state": "MN",
                        "zip": "89575-9808",
                        "lat": "14.23999706748873",
                        "lon": "128.96796497516334"
                      },
                      "name": {
                        "first": "Korey",
                        "middle": "R",
                        "last": "Orn"
                      },
                      "last_modified": "2012-06-29T19:21:50.785Z",
                      "id": "4fee004cca2e43cf2700000b"
                    },
                    "uri": "/v1.1/students/4fee004cca2e43cf2700000b"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "dob": "10/18/1998",
                      "frl_status": "N",
                      "gender": "F",
                      "grade": "8",
                      "hispanic_ethnicity": "N",
                      "race": "Caucasian",
                      "school": "4fee004cca2e43cf27000004",
                      "sis_id": "7921",
                      "state_id": "3432723",
                      "student_number": "49789",
                      "location": {
                        "address": "9629 Champlin Loop",
                        "city": "New Bradley",
                        "state": "MS",
                        "zip": "72776",
                        "lat": "-62.99142876639962",
                        "lon": "93.68024323135614"
                      },
                      "name": {
                        "first": "Horace",
                        "middle": "P",
                        "last": "Hermann"
                      },
                      "last_modified": "2012-06-29T19:21:50.809Z",
                      "id": "4fee004cca2e43cf2700000d"
                    },
                    "uri": "/v1.1/students/4fee004cca2e43cf2700000d"
                  }
                ],
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/students?limit=5"
                  },
                  {
                    "rel": "next",
                    "uri": "/v1.1/students?limit=5&page=2"
                  }
                ]
              }'

  STUDENT = '{
                "data": {
                  "district": "4fd43cc56d11340000000005",
                  "dob": "12/12/1998",
                  "frl_status": "Y",
                  "gender": "M",
                  "grade": "7",
                  "hispanic_ethnicity": "N",
                  "race": "Asian",
                  "school": "4fee004cca2e43cf27000003",
                  "sis_id": "1783",
                  "state_id": "2237504",
                  "student_number": "24772",
                  "location": {
                    "address": "9272 Ratke Haven",
                    "city": "North Maurinetown",
                    "state": "AP",
                    "zip": "22505-3577",
                    "lat": "-21.901449509896338",
                    "lon": "114.23126022331417"
                  },
                  "name": {
                    "first": "Constance",
                    "middle": "",
                    "last": "Roob"
                  },
                  "last_modified": "2012-06-29T19:21:50.709Z",
                  "id": "4fee004cca2e43cf27000005"
                },
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005"
                  },
                  {
                    "rel": "district",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/district"
                  },
                  {
                    "rel": "sections",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/sections"
                  },
                  {
                    "rel": "school",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/school"
                  },
                  {
                    "rel": "teachers",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/teachers"
                  },
                  {
                    "rel": "contacts",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/contacts"
                  },
                  {
                    "rel": "photo",
                    "uri": "/v1.1/students/4fee004cca2e43cf27000005/photo"
                  }
                ]
              }'

  STUDENT_SECTIONS = '{
                        "paging": {
                          "current": 1,
                          "total": 2,
                          "count": 4
                        },
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "grade": "7",
                              "name": "Planetary Chemistry 1(B)",
                              "school": "4fee004cca2e43cf27000003",
                              "section_number": "83",
                              "sis_id": "9717",
                              "subject": "science",
                              "teacher": "4fee004dca2e43cf270007d9",
                              "students": [
                                "4fee004cca2e43cf27000121",
                                "4fee004cca2e43cf2700033f",
                                "4fee004dca2e43cf270006ef",
                                "4fee004cca2e43cf270001f5",
                                "4fee004dca2e43cf270007a1",
                                "4fee004cca2e43cf27000107",
                                "4fee004dca2e43cf27000683",
                                "4fee004dca2e43cf270004a1",
                                "4fee004cca2e43cf270000b9",
                                "4fee004cca2e43cf27000079",
                                "4fee004cca2e43cf27000329",
                                "4fee004dca2e43cf27000467",
                                "4fee004cca2e43cf27000005",
                                "4fee004dca2e43cf27000789",
                                "4fee004cca2e43cf270002a5",
                                "4fee004dca2e43cf27000499",
                                "4fee004cca2e43cf27000439",
                                "4fee004cca2e43cf2700016d",
                                "4fee004dca2e43cf2700055f",
                                "4fee004dca2e43cf270005d9",
                                "4fee004cca2e43cf27000107",
                                "4fee004dca2e43cf2700062b",
                                "4fee004dca2e43cf270007c5",
                                "4fee004dca2e43cf270007ad",
                                "4fee004dca2e43cf27000473",
                                "4fee004cca2e43cf27000401"
                              ],
                              "last_modified": "2012-06-29T19:22:18.179Z",
                              "id": "4fee004dca2e43cf27000818"
                            },
                            "uri": "/v1.1/sections/4fee004dca2e43cf27000818"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "grade": "8",
                              "name": "Earth Physics 5(A)",
                              "school": "4fee004cca2e43cf27000003",
                              "section_number": "90",
                              "sis_id": "3899",
                              "subject": "science",
                              "teacher": "4fee004dca2e43cf270007f2",
                              "students": [
                                "4fee004cca2e43cf270003db",
                                "4fee004cca2e43cf27000291",
                                "4fee004cca2e43cf270001c9",
                                "4fee004cca2e43cf27000143",
                                "4fee004dca2e43cf27000709",
                                "4fee004cca2e43cf2700014f",
                                "4fee004cca2e43cf27000439",
                                "4fee004dca2e43cf2700072d",
                                "4fee004dca2e43cf27000581",
                                "4fee004cca2e43cf27000421",
                                "4fee004dca2e43cf270004d9",
                                "4fee004dca2e43cf270006b9",
                                "4fee004cca2e43cf27000005",
                                "4fee004dca2e43cf270005c3",
                                "4fee004cca2e43cf27000267",
                                "4fee004dca2e43cf27000631",
                                "4fee004cca2e43cf27000191",
                                "4fee004dca2e43cf270004a1",
                                "4fee004cca2e43cf270000e5",
                                "4fee004cca2e43cf2700027b",
                                "4fee004dca2e43cf27000709",
                                "4fee004dca2e43cf270004e5",
                                "4fee004cca2e43cf270002b3",
                                "4fee004dca2e43cf27000563",
                                "4fee004cca2e43cf2700007d",
                                "4fee004cca2e43cf27000353"
                              ],
                              "last_modified": "2012-06-29T19:22:18.329Z",
                              "id": "4fee004dca2e43cf2700081e"
                            },
                            "uri": "/v1.1/sections/4fee004dca2e43cf2700081e"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005/sections?limit=2"
                          },
                          {
                            "rel": "next",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005/sections?limit=2&page=2"
                          }
                        ]
                      }'

  STUDENT_SCHOOL = '{
                      "data": {
                        "district": "4fd43cc56d11340000000005",
                        "high_grade": "8",
                        "low_grade": "6",
                        "name": "Clever High School",
                        "nces_id": "44270647",
                        "phone": "1-895-295-3507",
                        "school_number": "481",
                        "sis_id": "4083",
                        "location": {
                          "address": "9538 Leffler Forks",
                          "city": "Gwendolynville",
                          "state": "NH",
                          "zip": "58530-7427"
                        },
                        "principal": {
                          "name": "Krystal Kiehn",
                          "email": "Domingo_Williamson@mailinator.com"
                        },
                        "last_modified": "2012-06-29T19:21:50.661Z",
                        "id": "4fee004cca2e43cf27000003"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000003"
                        }
                      ]
                    }'

  STUDENT_DISTRICT = '{
                        "data": {
                          "name": "Demo District",
                          "id": "4fd43cc56d11340000000005"
                        },
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/districts/4fd43cc56d11340000000005"
                          }
                        ]
                      }'

  STUDENT_TEACHERS = '{
                        "paging": {
                          "current": 1,
                          "total": 2,
                          "count": 4
                        },
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "email": "lillian@mailinator.com",
                              "school": "4fee004cca2e43cf27000003",
                              "sis_id": "5320",
                              "title": "Harness Ubiquitous Convergence Manager",
                              "name": {
                                "first": "Bridget",
                                "middle": "B",
                                "last": "Kulas"
                              },
                              "last_modified": "2012-06-29T19:22:16.645Z",
                              "id": "4fee004dca2e43cf270007d9"
                            },
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007d9"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "email": "charlie.jacobi@mailinator.com",
                              "school": "4fee004cca2e43cf27000003",
                              "sis_id": "7934",
                              "title": "Iterate Wireless Users Manager",
                              "name": {
                                "first": "Noah",
                                "middle": "I",
                                "last": "Schimmel"
                              },
                              "last_modified": "2012-06-29T19:22:16.893Z",
                              "id": "4fee004dca2e43cf270007e3"
                            },
                            "uri": "/v1.1/teachers/4fee004dca2e43cf270007e3"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005/teachers?limit=2"
                          },
                          {
                            "rel": "next",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005/teachers?limit=2&page=2"
                          }
                        ]
                      }'

  STUDENT_CONTACTS = '{
                        "data": [
                          {
                            "email": "Samanta.Schaefer@mailinator.com",
                            "name": "Issac Roob",
                            "phone": "597.747.8259",
                            "phone_type": "home",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "guardian",
                            "id": "4fee004dca2e43cf27000931"
                          },
                          {
                            "email": "Dianna.Donnelly@mailinator.com",
                            "name": "Hollis Roob",
                            "phone": "(813)513-5162 x057",
                            "phone_type": "cell",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "mother",
                            "id": "4fee004dca2e43cf27000976"
                          },
                          {
                            "email": "Elna@mailinator.com",
                            "name": "Cathryn Roob",
                            "phone": "1-695-828-3812 x167",
                            "phone_type": "emergency",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "father",
                            "id": "4fee004eca2e43cf270009f8"
                          },
                          {
                            "email": "Bret@mailinator.com",
                            "name": "Rodger Roob",
                            "phone": "1-254-341-9642 x4259",
                            "phone_type": "emergency",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "father",
                            "id": "4fee004eca2e43cf27000b93"
                          },
                          {
                            "email": "Zack@mailinator.com",
                            "name": "Kailyn Roob",
                            "phone": "(505)219-6289",
                            "phone_type": "cell",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "mother",
                            "id": "4fee004eca2e43cf27000d70"
                          },
                          {
                            "email": "Milton@mailinator.com",
                            "name": "Cecile Roob",
                            "phone": "571.073.1911 x4786",
                            "phone_type": "emergency",
                            "student": "4fee004cca2e43cf27000005",
                            "type": "father",
                            "id": "4fee004eca2e43cf27001407"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005/contacts"
                          },
                          {
                            "rel": "student",
                            "uri": "/v1.1/students/4fee004cca2e43cf27000005"
                          }
                        ]
                      }'

  STUDENT_PHOTO = '{
                    "data": {
                      "data": "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQIBAQEBAQIBAQECAgICAgICAgIDAwQDAwMDAwICAwQDAwQEBAQEAgMFBQQEBQQEBAT/2wBDAQEBAQEBAQIBAQIEAwIDBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAT/wAARCAEKAQoDAREAAhEBAxEB/8QAHgABAAAHAQEBAAAAAAAAAAAAAAECAwYHCAkFBAr/xABHEAABAwMDAQUFBAgDBAsBAAABAAIDBAURBhIhMQcTIkFRCDJhcZEUI0KBFRZSU5KhscEkYnIXJUOCJjM0NkRzdrKz0fDh/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAIDAQQFBgf/xAA2EQACAgEDAgQEAwgCAwEAAAAAAQIDEQQSIQUxEzJR8CJBYYEUM5EGIyQ0Q3GxwUKhFeHxRP/aAAwDAQACEQMRAD8A/TQvrB5cIAgCAIAgCAIAgCbtvxAKap8VcgiDGHESEbGg78EAt+Z8lXfCFFbkvkNu/wCEyhoLsg1Zr18c9NTtttldL4rtXh5jmaMk9yAPvDwMBvGfe8OVxNV1iNEMZ5NzT0SXc3f0N2P6U0U2GWmtUVbcmtxNeriGVFdK4tLXYiA7qIckZjySDz5ryGr6jZqptTfD+R2K47YmWo2hm1rYmxMHAaGj+WOAtIsKyAICnKwvjewHBcwtB9MqM1mODDWVgxb2taTOrtBXy3Mie+tjgbc6IsjL6kVFMWPAjaOS57YywAcnd8V0NBeqrk5f29/c1rofu3k5hsLgCCXeJzXgPOHOa5m8vA9MlvPTxD1X0HTWKcODzdscahMmWyXBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBACQASSGgDJJ4AA6lQsScXkw2kssle5rAC8hjSMEuODgjP9Mn8srZhNV15Mx+LymzvY32Jm/up9TatpZIrHJh1ptAa4S3HaCRNKccR5wQ7oSAPNeM6t1aTm6IS5NvTUWO1OUeDeGioKaihhpqSmjpKanZ3cVNDEIoYeCPDxg+n5ryeottm/U7EKlA9RQjBL4n3LQrAEAQEHHAJWUsvBlLLwUJHbo3tIwHDbkeZPAUIzatSIXJKt8nMPtd0s7SmubzQQxiOgr6599twDcMZFVOke2KP1ZG4yx8cDumhe76Rc5Vrczzd8MXbsGM13PEh6kQpgIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAix53xFgMjnTMZHE0b3SuLgAxo8ySQA0cknCsqhGc9suxCcd8dpsN2F9lLNWVzNVX2ldJpqJwnt29hIv9QHO2yEdO5jLT4hwTGWrzHW+rPRN0VM29JRKPDN+6djWNYxjAxrB3bA2Luo2NaMBjG+QHHIXhMK2zxWd6PlR9yuJBAEAQBASvOGnKynh5Mp4eT5pOInuG4hoLnNawyOIGCQ1o5JwDgDnKqlFuaZCazFo1e9pTR0lz0vQaqghH23TdS5tzdFA8ukp5w1jznHuMkDXgngB7yvR9IvitQqpvhnKvqe18GjIc1xO1zHbSA4DnBOcA/Pa7+E+i9jXRGTUkc9prhkyu8rUSO5ZwFIyEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBEML8NG4kkNAaMuOT0Cyk5PCMNpdy7dBaKrNf6lodNUb5IYZ90twqYmF89FSMw2eoGOhaHNa13TvJIxnLgDzup61aHTysTxL5F+ng7LEkdQ7NZaKy0dJbrbTiloKWHu4KZrNjIvdHAxx7ufm8r51rbJ62bnY+TtVVJctHtDIcBg4yecfBa9cHX8K7G18O3PzKqtIhAEAQBAQIyCPVAUwXNIGHEZxwEB8N1oaW622sttbAJ6Stp3U9THI3c2Rrhg8JC+dFqsj8iu6vNTSxk5Za80fU6J1VdNP1A3RUc4mtczQS2qpJWkwvJ6EhjWAkfiMg8ivfdG1rvhmZwroSi8YLP9P8AM3cPiPVd2zYnlM0IycbUmFXlepsJN9gsb45xkBSAQBAEAQBAEAQBAEAQBAEAQBAEAQBTrTcsIw3t5IO2Bjy8Asawufl/dgAAnJd5AYyT5AKtWeFY2Vxj4kuToD7OmhBpjSkN+r4wL1qZgnkfJH3c0NMzcaaMZ5BkD3zvb+09oPuLwnXtc9Rq/Dfb37/U9BpdLGEVYjZBcM3QgCAIAgCAIAgCAoVAzBMC5wBYQ4szvAPXbjnOM4+KqubjW2ga9dvHZxJrHTrbzQwx/rFYYDUMLW7W1kW5n2iJzsfhYCWDzOcLtdH1ihNRmaGoryc+mctHhMYc8sbG7l7XDh0Z+I5ODzwvdUW1WpJs5MqGpbiYgtBJBAHBJGAFsPwYvaR5Q5xnA97bn8OcZx88cqfgwS3xRjxM/AQUQEAQBAEAQBAEAQBAEAQBAEAQBAEAU65bZZGM8EkpeGP7sN3kbWOk5bGTwHkee3O7Hntx5qmUd83nsVZlCXwo6HdinaxTazoWWW6SRUupbVEYp4wzuKa6QRNa1lRED0cQMlo6Brj06eC6zoZ1XyuSe3L/AOzs6TVOWIS4NghIw4w9h3DcMOzkeq451FysomBB6EH5HKPh4YIoAgCAIAgCAIAsNKSwwfPMC5haA7qOACGuw4HBIB4PQ8dCVXl1yWxEZRUlhmEa32f+zmuu1wu9Zbq+R9xrftk8EdxqKenhkPvd2GFpDTk8ghderqmorhiPcp/DKT5KNd7PfZdNTSQs0/WUj3Za2SnvlZM4kggPxJIWZGcjcOo9cLUl1zVqza8kLNJCSbiazdq3YhWaGpZNQ2itqLpYYqlkFb9tZ/vO3bm7GOmeBtcwuc1oI9QvV9J6tPUWRon8zl36ZwTlgwJ8/n6dei9MawQBAEAQBAEAQBAEAQBAEAQBAEAQBAMjkEZBBafzGP7oD7rZc6+xV9LdbTMaGvoZGvpqtlRjcWkH7xueQemD1ytfVaVa2iVEvmSg8STOj3ZR2lWztDsbZ3PihvlAAy8W8uayRryAz7Q1nUMf0AxjlfP+o6Sehsa+R29PqI7cGXWPiyA17CXP28PBO7bvx88HOPTlc5NyWWbj5eUV1kwEAQBAEAQBAEAQEr/dKAozOY5krA9pIbh/iH3eRwT6eq1p1ycsRIykorLMD9vurrZY9CXS0zSw1FzvzGUVro34cZC1zHySuHXAY1xB6bmhei6HRYtfXZ8lk0dVcpVSijna3O1rXAEta2LI94uDSS4/Dyz6r35xyZAEAQBAEAQBAEAQBAEAQBAEAQBAEALXHO0Z4zj5clASuI2udu4253Yy0Z4B+SOcqlvSIuSS4Lk0bqu86MvtHqCzvAmhez7VEJf8LcWc/du8jvGQD5dfJaGp0mn18Xukt3p8xVe42JM6Y6L1TadY2m2ahtVSHU9TFJTsic8GSneHHvaaRueHMMQc0nks56HK+da6uWk1PhTWF6nqKpRlVnPJfm4eo+qpUoy7MkRUjGVnAQyEAQBAEAQBAUphljhzgjDsDJI8wPiRkD5qMs7XgGMO1O76wsGl6q5aPtFPdq2ncDUNlLnSW+ADAljYOZHNJ3HqGgEngZW3oY1WWKNrNTU7lFtHNW63276juEl0vNwrLrc6lpZUVFVmMtDTnYxvuuDcfg6Y54yvpGj0Olq03i1tZXY4ls5Z+LsebkHBB4JwCVs7ZehWpxfZhQ3JPGSQWQEAQBAEAQBAEAQBAEAQBAEAQBDDW7gg7G0ucCWtG84OOBz/AGUJrjJW5bXtRfvZzoG6doupG2igkdBR0zY6q73QhzG0FO8POxhA994Y9jfjk+RXJ6j1RaXTyknybOm0njWLJmTto0F2bdnumrbR2ije3VVTNCylrpq2Wouk1Oxr2yyTMPhI4ezpw4+oXL6PqbdRqfFl2Lr9JGqWV3MddkXaVL2fag21L5Bpu5ubDcKUP3x0+7hs7PJpDywvPk0OJ4C2OvdK/Exd0UTrvVccI6Q0dVS1sMFRSVME8FRAKqmfDK2Rs0TwMPBBOW8jkcLxboVMnHB16bd0cHpnq35/2KJp9jLXxpkVkmEAQBAEAQBAQcNwI9UHYoz8MJG8nBwGAF35Z4+vCosn4dikQsW6ODXLX/YHpvVT6y6Whr9OXyoeHvkpaUy2yrkLxl01O3nvCCQZGeHnK9BoeuTqShnMf8HOu0viwcMGmutdBar0HWik1Db5IaeV5ZTXIP76hrPQRvHDT08JOV6vRdZruWMnOt0bpWUWeWPBdlrht97LT4c9MroxUJ/vEainJWqLJVM2AgCAIAgCAIAgCAIAgCAIAsNpLLARSjLszLTXDHp8TgLE3iLZKKaeWQJIw5jmh7XjugeWvkadzWH5kAEehKzYl4OUaclKVi2o6PdimkafROh4KmrbHFXXaI3241EzRDLFHKGugieTggRxsGQejnv6cr5z1K16nU+AnlZ7HotMlXRva5RpD2oaqqtY60u94me8U7aw0FoieMRR0cQ2tc3y3PLYXHHm5/oV7Xp2k02l0EcSW7/s0NTdZN8IsCQmHbvbgkZ2SDb3g6uAz6jK6dFlN2nlXY1k19s9raRt97N/aY50rNBXyp2De6bT0kkveSPJY5xppHfhAa1xa09SAAvE9c0bhPxao8f6OlpLoRW2bwzc2OVkrWvje17HDc1zHBwcCMggheXj8MnGXDOmmmsoqq0yEAQBAEAQBAFhtpcAgRkEeqrlWp8oFFzBtGSTgg42E5wR5KjwZRlmIWE8ni3uy2+826rttzpG1lFWMPf00wbI1oPUx8ZDm+8MeYWxp9VKqzGcFN9fiQwjmx2pdn83Z5qQ22PvX2aua+ss1VITmZgcA+NxPBcwuGR1HGV7/pWtV8FFvk5F2l2J2GN13TUCAIAgCAIAgCAIAgCAIAgB4BJ4ABcSeQABkk/JYcFNbWN2z4iDSH7NpDt7N7dp3bh6j1HxWIxrreDOXPlEckbS3q/Pdk8tf5dfT1Pl1W1CFdnD7GM7/wB0Z67HuyCt1jcaLUde6W22CgrYpjOY9s17lgeJTTsa7juwWgmQZDgCM8ryHW+rrSN0wfBuaeiS7m/lRb4aqlfRzsD6aanfSTtBdE9zX48IcOgDdw+gXiPGk7fxke6OvGtOG1lr2ns90ZY2xQ2rS1nghJc6SU2yKSpcSDy6V538n0Hn6K19T1cpbnkj+FR6tx0np24U0tPVWO1TwvjLJIn2+N+9n4mgeuCcHyOCrtN1LVeIstohPTqMXLBzT1fpLUGiL7UMktV1s8ENfK7T9RJShkT2PlDoTFWN4zjDSM5AJC9P/wCTjZW4tnHnpZSs3JHQ/sz1VW6t0xabvcbdXW6ufD3Fxgq6J9A3v2N++ljDmjdG87XscOD3zwM7Djy19UVduR3aHiraZIa5rxlrg4eoOQoFpMgCAIAgCAIAgCAgen5j+qApysL2uAOC9pbuA6ZHBWlZW3ZkGtPtMWNtfoinvQgDZbHd4pRKWeNkNUBC8E+X3ros588eq9T0OxQ1UYev+v8A1/g1tX/LyNC3AtIDmlpOcAjBOMA/TI+q9tDzI4BBXAIAgCAIAgCAIAgCAIAsZXqHx3Ig4LSC4OHLS1pfIHfh2t8znGB6qF1jqrdhhwc1tRtX2W+z7Lc4jf8AXdNPTw1TSaOyGXua/dg/f1Rx4HN4cyPAw9rcjGV43XdWsjY4wZ19Np63D4u551H7Nl5br39F1QdJo/a6vN4GQ+WIv3fZ3N8qh2Axzhxtc4hSfW7IaZyi+TXWm/i00uDdq3WektdPSUNBDFTUlHA2lgijZtayFow1nXk5wS7zXlNROzWy3TOrCqMOx6wy3IIO3z44KsilXDDLM7exHczONvPpgZWU1L5De/qQc5gByNvlkt4CzLszGXLjk+GSKnlI7yNkzWnPdTsZJEOOrTgkevHoqfG2cNmPCjjOCc1EUQ3ufEyCElksjpS2KHA5DuA0Y884wrK5TtliKyRwoPPyPMs2pLRf5q2Oz18V1Zbak0tZVUj2zUUMoaHGLvG5aXgOaS3OQHAq2UJwe2awyxST7FxqJkIAgCAIAgCAIAgCAxt2tWn9NdnerqQtcS2zSVkbWjLnOpi2paB8cxBb3S7VVr63LhGrrOdPLBy2aSe87w7ix5i4/A94ZI5p+IweOq+iV/F8UexwSZXLlZQXKyggCAIAgCAIAgCAIAsSWVglF7ZZIFwaMkgDpknA9Fjw1jJTZLdI9vT+m7pq28Umm7PSmpuNxeadjXZa2kHR9Q8j3RDneSfNoHmud1DWRq00oyZu6WGGmjrJE0hzSWyHfucJHMLOp3bSMcYBxk/sr53qEprCOrXH4k2egAQGj0P/ANqKWFg2NqzuJlkkSuJDSRknywMlYaUlhgkHHidwc9COT/8AsqqVnh/CChPUQRQyyySNjihBdLJv2CING5xJHTABJ+GVmtyulsS4ZXOyMIuRhu+duvZhbRPBJe3XV8YJMVto5biyQsOSBLjusjGQC7kgAZJAPQq6RZbLKRqT1qSwjU7tj7Xma8mooLN+k6KyW2jqRUSVlY+jfdHufG9rnwR+FjWNa52Xke6vT9P6U9IvFs98GtbqvEWw297EtN/qz2fafpZGhtbcYnXq4YJDzLUl0hbIHeIuY0xsIPILcHovLa+e7Vs3tL5X79TMOMFoHxWm2tyRsPzInUyQQBAEAQBAEAQBAfLWRRz0s8E0bZIZozFNG7lr2O4eD8wSpwbU00VXx3VNHIu9W2WyXq7Wao5qLXXy0EzvKWaCWWB0g+BbEDn/ADj1C+ldNsUtGpJnnNRBxlk81blazWyMPKgsEggCAIAgCAIAgCk4SXdAKDaj3IzeFwTNeIyJHbcs8bA/mJzhy1rx+wTgOA52k4Vkp1eFhS5NXf8AFj5G6Hsux6XioLqKUMbqlkvcVYqXA1TqMtL4DBk5EZdu3MHiBa3d5LwfWXa28L4Tu6Nwyop8m3jXNOMOaSefCcg44Xn021ydVRSJRuLsnIHU+QWSRUQxlepTc5pBw4ZyB1+KxmPqZKU7xscByc7duQNx/Z/NUOErLO3BVbLbHJpz259sTQ2p0PpasZK4uMF9ukTwI6XB3fY43jgvG0bvPGfivXdL6YmlZYv7I5dtqaxk087sMIaSTsyAQ/cHk9SV62nS1VxyzTnh8pkXEhjsNc87SBGG7zICMFoHnnpj4qjUSs8iXBVCLViaOs2lLhFdLBp65w7GtuNkpq0xxY2QCaBryOOB4gR82n0XzTXRktU8+rPQ6XyF0n3m/mteTfjJF78yJleSCAIAgCAIAgCAICSQEsIAHOBz05KjOW1ZMNJ8M5s9v9kNn7TrvURsDaa/UsF6iAGA0yNMUpH+qSB7v+Ze96JYp6JRTy+36HH19ab3Iwwu/W1GDjLv/wDTmQbT2sKJYEAQBAEAQBAFh7sfB3MNuKyiAIPQgnjoc9QHD6ggj4FVeLdGW2RXvk2TFpABIwCQBjoc8BWKtP47X8Idc5rk+63Wq53apdRWq3XC51bRl9JbKWSrrGjGT4GNLhxny6LX1F2irWck4aWX3Mz9nfZBr6XV9gqqiyXfTNFDWMrau+GJlNP3UYMncv3ESePbsIx0ec8ZXnOqa7S3aaddT5Z0dJQ67YyZ0SaH72E94AA4YyNuB0z8SvKHZK54B+ShZPZByMNZWGSlzXNzuADm5BJHT1WurbJdhtS+Raer9Yae0bbH3G/XKmoIf+DHJ97UVbsjDIYh43uP+UENGXOw0Ejb0untvujXjuVXWKFbkmaR6/8AaC1FqU1Nr08x2mrRJE6F0zsVd2r2OBa4OczMUG4EjMZLhuOcDK9VoukqLW9HHu1W411awtc1w34wY3iWUzS/teJ34jnHiXqKNN4MMml4jlLBVVxMhkjxDqwh+fTac/2WJdmSh5kdLewuuFx7LtKzyk9/HSz24gnxBtJWVMDOP9AaR8HBfNepL+Lk/r/o7ml8r9+pmBv4PzXNf5q9+ptFRWgIAgCAIAgCAIAgKUrQWHkgAh3ALsAEHgD5Ku1boYMNZWGam+1BpeWqs9m1VTxDFkkfQXKVlM4Ew1MkYpy52MAMka5vPGaho6uGfSdC1cdPN1t/2/2aGqg2aTkOBwQAcb8HrtzjPyzxleyh+8asZx5RxPKIK4BAEAQBAEAWUm+wIMe3cxwcx7WSt3eNwDfEM5LXA8deoWXvrW9LsNu74TYzs57BKbXWl7VqJutJ4HVjHMmoW2Jrp6UxFzA0yGpa8ZZ3Lvi2Rp6OC8n1PrV+nva2cI3adMpSwZht3ssaLgfFLc7tfrhKx257Y3w0MU2OrXENfIGnoQHjIJByCVzLf2h1N9DpisJ/qdGOlhFYRnuw6YsunKSO3WSy0VpoYxtMNDTx0zH4z4jty57s45efj1XIsvvseXIsjRBcsuNoAIdgtA65G0DjC1FGanvnLguVcIrK7lbIPQgq5NPsChO+JsTjJtLBjIJwOox/PCyoKx7GRnLZFz9DXjX3b7Y9F32SwRW6qvU8NM+eplpayNsFO4AhsBGepcWNcOrQ7JXZ0XSJ3rKj/wBHOu1eOEaT6w1heNa3qtvd2nMslTVA0tI55MNpgDDthjHQkHGX+fK9Xo+l1aeUZNdjn2aiU3sZaq7ChWnkg4pvPAUpSSi1kw4QXbuRALjtAJc4hrQBkuJ4ACpXPYwQc8MZK52QIw6KXAyQ4NOWH0OM8dVCycaq3Za8L6k64uUlt5OjvYDaq2zdnNlo6+D7LNVSVlzgjc4l5hlqHujJB5AIc1wPQghfNeoSjPVOUHlHc00XGLyZwAPg4PGc/BaLzvTNknUwEAQBAEAQBAEAQEHZwcZz8OqA8W/2u33yyXO1XOMSW+4UUlLVNI3FrC05cBg+JvvD4tClVbKu1YKbq1KLfzOXGtNIXXQt+qLBdIyzu4i6jqGgllfTh26CRrvNrYwQ8jgPGDyvonT9ZpbNOszW4491M1mWC0MhwBa4OBbuBByCOmfkujF7lmPJqvjuRWXw8Mzh4yEMBAEAQDaH5aQHZ6NIYRny94hvXHJPCjO3wouRCLw9rM9af9nntCvEFJXTPtFrpZ6dlXE2uuDqmocMh3DIQWjI8j64Xm9R1912utM3qtO296Nu+yvs8d2aWE6eFykuzZ7lLdDUCl+wsge+KGIxhrSdzcRcF+OnyXlOo65am1zXc6NNTi02ZZPvN/NasEsZN1fMmUzBK/3SoyipLDMSWVgANaM54PmTwscQXAWfmWrrWqqKLR2pqy3zthrqWx1VRRy7Q/EkUL5GgD5t/L8ldpFKeojnsUar8lnJ6Splqpp6uokfUVNZM6rrKmWTvZJZZQx7nDzEfoehOF9G6Y/Bjg4M/MyRdGx7p7mU/wBQKJYFlRU3tYJXjLHNyG72mPcXbC3d4cg+ozkfHCtnX4UXJFTWZ4NnvZ+7K6O/93rS/wBIKy20kwpbJRSZcJ5YTuNRM3zIxnB64A5zheO631Jyqlpk+WdfRVtSTwb1QtDdowcABzSIzE3ptwG+QA/D+a8kdY+pAEAQBAEAQBAEAQBAFGflYJHgFpBGcEOx8iCP6KMZ/Jgx12g9n9k1/aH2u7UmZmuM1ur44z9ptkxztkD/AMTScAs9HH8tvT3OmxTK7Y7oNI5uau0zddIagrdP3eHuqmhjDmnYY4q2NznBlVGT1Dtrm4Ho70K970nWRnBNM89qKmpYLaXUs/MQT+BoIQCAIAgIgAluemcknoB6qiUXPTtspabtSR1R7O6t9fojRdfI1wfV6ZoppQRy10lLHI4H4hxx818z6jXJat4PUaWCVGfmX4C0nhuPjha3hRfLL2sEyyk09vyMBTBK/wB0/wA1lNJ5ZmOM8lHeGtORkE9RyecAYHmVp+I7btsTM1hmpHtEdqTaWCXs/sVW11ZWQhuoJoZAW09O5wIjDgRte4gNcMghrjyOo9Z0rp7aV9i/sc3UXQcHHdyazXiq0VPpOwRW2Cqi1PA0/pGZ0RZBK1rtrQeAATuMgGB/1uMAMbHD6WqNtdjyuPl/s5MsOXBZGDgHBwXbQccE+g+K6EWscsqw9+QQWnBBBzjB4OVImQUJz2R3GG0lljgFpceN48s45GFe7VOnLZCrbKfLOifs5VME/Zdp+KOUOmo5a2CrY0DdvfWTzMcfTEb428/s4XzPqkJrVtvsel08Eq8oz5kccjnpz1XPLyKAIAgCAIAgCAIAgCAKFibg8ALXhGaeMAke7awuz0GVm6Vkavh7mUsvBp/7Uuno5rdp3VUUAE1JXPstVNG3cZY5ozLA6Qjo1hhlAJ4zO31GfT9A1DTUJs5Gujjk0yXuovcso5oWQEAQBAQOCMHgHwnHx4Uf/wA/6/7I0+Y6ddi1T9r7M9GSuLg6K0CnIPUdzJLT4Py7r+S+bdRX8XJ/X/R6ej8pmV1pEwgCApyEhp29dwB+oz/JGs8Myll4Rirtc1hLo3Qt0vFBKyO5VEkdvtTnkCPvJXt4b6nu+8eMeTCegJV/StErdYVX24raZzPqJ5aueaqqp5ZqmoldU1M8zjLLUTuPiBPkwAnHlkBfTY6dUqJ5u17rdzKGcc+Y5HGefL+eFKflYPas2nr1fv0g602yatNuhNZcHsa58cLQMkjGccKiU4QxveM8fcHhse0twwEMJLgCdzw4HDgT9VOHmQJlK2O6DQ27vhA2uy0kbXAxvIPia1wLXEfEAkj4hXV6aMq8M1pPw5cI2P8AZx1/BYdQO0vdZ2xUGoJO8pHSPEbaeuDTHHGM48MrPC39qRzGjJIC8l1zRwjVK6Pdf494O3otRKUlXjg36a9m5rGua7IJDd2XDacO4+BIB9CV5I6xXQBAEAQBAEAQBAEAQBAEBJISGOIxkdM8hVXNqt4GccmsntM3alpND0dqkcw1l0vUDoIsjvWNije97tvXHhxnpkhd3otDnapPt7Zx9bZueDQ1fSIQUNO8GhjjIWuYCAIAgG3cHAkAFp2knHOOP5ph+B7+pCpqMsM6UdgTw/ss0q/e0uMVY0gEEjFxrDj6EH81806ksaySfr/o9RTJOozQtImEBA9D1/LqgPNp7hRVrZzR1MM/2aTuZzDM2Tu3g8sfgnDvgeUbceSUfMaze1NOIdJ6aomkNZJqMTuaTguMNLUMYMfEPeR8GH0K7vQGvxO+XBytc5dkaNr3lkt0o7eUcdZy9xAhrt2/3Q0uOQXYwCR/RV2SiovLJl1W+s1jpOgZcKP9JWa16ip3Uzal0BdbroCDkMeRhxA5w0nGFqOzT2SUZNNoFqF2xuZJg8dHPe3uxkn+5OPzV8Gty5BO5jgdjmkHG4hwwceqsm/hMxeHkuPSujL7rW4Nt2n7bLWT5HfuIdFS0rOpkmkxtY0YyNxG4gNHLgFq6jqdemrxKSTJwodkllG8XZv2Cae0b3V0vDGX7UkTmyRVk0JFupXt5YYIfeBa7HiPpwvC9Q6rbqZutdmdqjSwrW9dzYCKNre7DWFrQHHIaWDLiS/IPPJ5XONo+pAEAQBAEAQBAEAQBAEAQHl3ipdSWytqY6Sornww720VIwSVFVyMxtB48XQnyBJ8lKEYykoy7Fdu7Y9vc5udqU/aBqW+PvupdLXu00MWaW1wVNofHRW2mB8DGz42ueTjLh1zhe26S9FVBR3o4OohfOWdrMTZHPI8Pvf5fmuw9bCc9ieUY8OSrfAHIBHId7pHR3yVylF9mVPjuPMDzPAHqpEd8M4yRwfQ/RDKafYgsZXqZIt27mbum8Y+eRj+eFd/Rfv1KJfmo6Mezoc9leng8hxZPWgY/D/jJl806r/Ns9NpM+B7+pndc02QgKUrGvjexwy1zSHDqSD1wsp4eQWRpbQ1k0Z+l3Wg1Ydeqt1bVOqpXz7Hv48OenJUbp5g0YbwsmJPaVkjPZ/E10DnyS32BkJbCZjEIxUOfKXj3G48OTxmQDzC6vSXttTOfqJt9kaEAFwYRkiQkRkDIkIBJ2+uAD09F9Cp1FFdOZs5DjNzyyQjvAWDcS47SGuG4H0//ixK/RWvlozh+hcNy1TqG6Wm3WG5Vz57XZGl9DAYRGKfIwcn5KMNPoIzdixuGH6HhU+Z5NsDy5zGh8roSHvjY47dx9Ac4yfVSt1GirzyMP0Ms9l/ZHX9oVQKyRs9usVFXuiqK7a59ZWANcdsTvdyXbRno3OT0XH13V9LTTKVb5LaYOViizoHpfTFm0lbqa0WS3/YqSMBxcWtfNO/b4nzSDlzzzl3u+Q6rwWt1stTJub5OtXXtWX3LtHvN+DefgowxtN1PhoqKRgIAgCAIAgCAIAgCAIAgCAkkztOM56gNOCVhpPhgpPY543bAZG8sa8N8GeCA7B6jIPzUsqP9iMo7ix9RaA0jqION60zarhI8bTUGk+z1DM4yRMz7z8wPnxlWrqk6PLJ/wCff6lLoT7mBNU+y7aKoSzaRus9pkbl8VvukIuVDK4ngNkz3jBzxx1xnjK62k69J+c1btG5djVjV2iNT6GqfsupLRLROmlMVNXiT7RQV2OcRPHAOOducr1Wj6rXqEop5Rz56CVb34LPyc4LS358FdRQrmsxK3BxJlnwX9CJA9Wf+az/AN7VL+i/fqUyi3Yng6M+zmD/ALLrEcHH2mtGccf9snXzTqv82z02kkvA2meFzTYCw20uAFDdP0BTk9wkE5GHZAyTgg4/NMyk8SXBlLLwzzKqlpa2nkgqo2Oied8sMsUdTG74Yc1zcHz48z81ZGcqlmBXKqMs8ltwaE0RSVsNbR6M03TVu8ubWUljoo5WEgguDyxrwSCRlnqp/wDkdTJbJN4/uyv8NU1yQ1HpnSl9hhqNUWm2V9PbHF9LPXtEjYARgk5YABh3xHI9cqVWouj5ZB6WvlM8y3aD7MbhTMqbfovRlZTyEs+1x2KiqozjqO82bifLgefKtnrNVjDk/wBTD01P/FlwUWkNL2epjrrRpqw2ytYzu4Z7fZ6eifG1wLC3vGRb28OOceWc8ZWrLUaicdsmyX4avnJ7sFM2JwLImw73nvI4osRh2CS/OAeemVTZGyaeWZjRXCe6J9zW4PU8fDqoQ06WGy0qK9YT2oBSAQBAEAQBAEAQBAEAQBAEAQw2ksslcXDGPz4ysNJ8MksZ5KQLnOw4EcZ5GMf/ALhVT01clwyUlFRyiZ7QGktaCcgAHLh1Hks10xr8pTGW59i375ZLZfbdWWu8URrKCqbianma18TdxHMbsZDgcOGPMY81etTPTvxod0YnBSjtRzY7TtCydn2q6qyRF7rVUQmvtMsgJc+IvALC7zc0kZHkvd9C19l8V4hy9RBQeWY7Xqfg+hzyBxjOQ0NIfknAG05/stb+i/fqDo77O7Czsp0uSW5lfWyuHQnNbUD+rT9F806r/NyO5pjOi5puBAEBI8Atw4ZGRxjPmEBYdhbrv9M6gk1E+3myvnEVhgo2YqI2Z8T3n4NyeepGPNAVdR0N6gkZe7C4VNbTRxx1NqrHbaS8RB7ctYf+HMMucw/thueMrDgprayMpbVk8+2Xew69slbQkvAkH6LvdpqGd1drfII4u8p6hp53+FwEvunAA5VirdayiuNniPDPf01p+3aaoqW0WxgbT0bCyEmNveRREEthe4HO4HB3Ec4VDubs2MsVUY/Ei61YSCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAICVwBB4z6fPyVdvkMrujRz2qnRfrFpdoZ99+h6hz3ee0zsDSfnscB67T6Few/Z75+/Q5Ou7Gqi9ocwleHFkgZw/u3d2C4AbsHbknyzjPwyq8rwml3B0O7BdUaZqNF2HT1HdaD9MUDJRXW4kUNU6WWSWoe5kLyHPZmR3iYCOCvnPV9NqFqXa4Pb6nZ09kFw2bACSMjIewjJbkOBGRyR+WD9Fymmu5uqUZdmTMeyQZY9rxnGWuDgob45xkkTLKafYw2l3CyZCApSjdG5pG4EYcPMjzUJzcI7kYaUlhmMNV6Jqa+sbqTTNW6zavpoxDDc5RvpLpCCP8LXRN4dEWlwBA3tJa48Aq2q3fHDIbFHmJDROu479cK6wXej/AFe1XZhsuliq8lxbw1tTRSn3qd5xtHJGQsSoxLxEYVklxLhGU97N23c3d+zuG76KLaXctI5HqPqoqyDe1Pkzh4yRUzAQBAEAQBAEAQBAEAQBAEAQBAEAQBAEBA4Ayeg5VdvkBoP7UdR3mt7FT5yIdJse/Ds7HOq6jg+hx/Rex/Z6Eowc2uH/AOjka6SbNaF7DxIepziBaHYBO3PAdjOFUCoyV8L2zM71k8UrcSwT9047CHMcD6gtBBHORwQcEQsoWpg6n8xu2/EZm0n2+67006OGvqhq+hMm50d6a6OpgbggNZNnvXkHGDO97R1AGAR57WdBhj4Tap1bWDb7s+7Y9I68mhp4aptrvYaQbNdcRVZIa5zvs8+QyYAAkhmSACSvL6vpdtEXLadGnVqclDJmdz2NIDntaTwAXAErmSjOKyjc4YLmhwBdgu4AJxnz4SuyUpYkNqzkiCCMggj1ByFeZIqu1ZgwSvBLSAA7IxgnGVGp7OGDGeuez+i1bBTVLZJ7TfrMTUWLUdJJ3VZa5fIDHvRYy17ZM5Y52OcFbtdmOGRmsxaLZ0f2gV1FeYtEdo0NPZ9VsaXUFxcwxWrVYa1+aime7DWP2sfluecOA5WLK4zjujyVRWzCZm8Oj3AA4cRkAnC0o6ZRn4iL93yyVCQMZOMnA+KvBFAEAQBAEAQBAEAQBAEAQBAEAQBAEAQFKUhsbiSA0eJxJwAAck5UJ9iMvKznR7RVYantMr4OXfYbdSUhAOe73wmoIPpgzD6hfROiJvpy+/8AlnD1PmMFrZ2SNULeAUZTlWt0e5lJN4YSNsrFiSM7En8LRO17o3Nex7mOY4Oa5hw9pHPhPUH0I5HUEHBGbNLTdBxkYU3B7kZo0Z29610oYqetmOorWXNjipLnLLJX0rR+zWDMrvlO948gBwR53WdFqlzA26tXJNZNntKe0R2fX0CGvqKjS9WZQHRXiBrKSZxHVlQwmMDPnKRnoOSF53UdI1FWZRhx/Y6NeqhJJSZnWiuNuuEbKmhraKuhe3cKqjnbPTvB6bXtJafqudKq2PE4tGwrIPsz0NzcZ3NwfPIwVTOUUsZJkypckgUnvad7WuaZAw+HqR8woq2a7GUvm+xZGrtF2bWVufb7vSkxMlNRQVEYMNwtlT3krm1MM3XOS0tZgjJGWkcLdptku5XNfCYqs+tr12b3Wl0l2nVMlVbauQUum9eyD/BV5OdsNazktnHALiSCfMnMku5OCshmvv8AT37/AEKk5RnyuDYOKWI7XNezD+T3bx3T2luRIB5A8DPxWlJOPm4Ng+3c09HA/msJp9gRRtLuAsKcW8JgKQCAIAgCAIAgCAIAgCAIAgCw2kssEMj1H1RtJZYTzyj5pXwyRSgyMLCO6fh4OM8Y+fP81mPx+TkhOcYxbbOXnazdI7z2iauuMT97HXt1BEWnc17KZggDx/lPcswenIX0npDjVoY1z4eDh6iScuDHi2fgNYK4BAEAQBARb194tPqMZ+XyPQ/ApiMuJ9iMntWT0rZer5Y5WzWS8XG0zb9z5bbc6ilY0+XDXB3PQ7eME7styta7p+k1C2yROu9xayZnsftGdo9lLI6+W3agY0APNzpIjO4Yx4JqcxZPmTKHnr1K5F/7M6ZwcoPk6C1rUe5lq1e1JZKwBl+0zdKB5c1sk1rnp7nTt56kT9y4DP7LZP7jj3/s9ZVlQ59+/QsjrvnkzHZO2Ts3vL2Mp9W26leeXRXWR1q/CeC6bawn/QSFyrel30pyUe3obMNVGcsMyNT3S3XCAz2+voq6E4DZaOdlbHknAOWErSso1UFlpr+6aL/Eh6njX+y2jUNor7ReKKG4W6picypp6ghrG+ZkY/Icxw6hzS3BA8TeopjbrNNPxprgi5Rn8K7mBqS5ah7C6iGgvJrNR9mEju5tN2cTJctItPAiqhjJjDnNYJHANG/gR8iTrU1rXRcm8SIxWxmx9uuNDdqOluFtqIauiradtTSVVPI2WCZruha4HB6+S598bK3hF6lxhHp7m5blzcuO1oznJGSQPofotdOUvMgRI8TTj1yVZClRluYJlcAgCAIAgCAIAgCAIAgCAICV/ulRlFSWGRn5WfO/JadpAO3aCWF+D0BwPiUlGMoOLJRxCLRpt2rdvlfDLftH6aozQ11HWz2KvvNZKyV8BbJUxOfAxvme5DSfw95nyXoej9MjOtXS+RytZblOJqC47nNk2v3yty5r5O8czHvyOd5ue7nHocr1dNDWGzmEVt7EApgIAgCAIAozg5x2oykm8MKFdMl/yMuEflgK2cpwhmLyyEllYCqjKVj+JCMccZIjG7xNDx5tIDgfyPCSqhGO9LLLbJuEcx7leGoqqaRs9LVupHxuyx0bnsfGTxkBrvj18uq1pV1z7xKnqrfR+8mUdH9quvLZfLNHNrG5VNpZdaX7bT3KokuTX04mZ3zGukaCwFgeOM9Vztd0qi3Ty2Qw39Db0WpnPUKFnCOjtXS01dTT0VVFHWQVLDHLHURtmimG4b/D/wAzR8C34LwNmoemnFQR3JLKeDW6tsWqOxGtqb5pKnrb/wBnNbUmtvekpS+qqdN5OZKqleMkt6Hux0AJ8l06lVqq2rH8RSm4z+LsZ40rrPT+r6OO4WK70VwjLGyyQsqo3V1EHjiKohBLon55w4AkBUXaadfLj/1x+pdvhnGS8GvBOOPyOVzFqMXKonh4yTrcMBAEAQBAEAQBAEAQBAEAQBRk2llAkfgAngEkfnysKxbopfMhZ5GcoO0Ij/aBrsggj9cLh08/8ZVH+hB/ML6V0GEPwD3dzz18k7C0V2Gor0KwsfB9AFSAgCAIAgCAIAgCAIAgBLmkOY4tc0hzXtGSwg+8B5kdQPPClGtXJxZOvzo6cdkWpLjqnRFku1zpRDUSNfStnjd3rLg2lIpn1RcOAZpGvcG+gJGcFfL+raaFeulCPyZ36fKZOq2h8EjHRslY5hbJHIzvWOaeHZZ+LjPh8+nmtKVnhSUmyyUVJYZpTr3s01N2aXuftA7PZrky1NmNVcLYAZpLUxzvHA9rciajkc4PIb4ogDkgArtabUx1MPCs7mjbW4PcjOvZZ2r2btEp4GEst+ooY+8rbS2UOY9oYW95TuJ8cBOSAMkHGei0tX0tUy/EtdiVGscpKmRmgOa7Ba5pB5BBznyK1TeJ0AQBAEAQBAEAQBAEAQBAEBRm6N/1D+oWtX+eiFnkZyf19/3+1z/6urv/AJ5V9S6Z/Knnb/zv1LVW+QCAIAgCAIAgCAIAgCAIAgIEkAuA93xn4BvJP5AEqM7fBg7c9idbSmmzqV2WW0WXQOkbWI3MdHY4Z5mFux8Uk3372OHUEGU5B5C+XdUvnPWylJcNneolFx4ZkVwyW8ZHOVqzirEsdi2UsNHzztD4nxkbw9u1zHMEjHA9QW+YI4I8xnHKnD93FfQltUuGa43vsAo5NX2jVmkLnJpgm5x195o6eJ7GuwQX/Zm/gD+WuY7ja52F0Za2dlDqmacdMoXqyHY2Qb77TtcTgtDtpaGgHz+a0DcPoQBAEAQBAEAQBAEAQBAEAQFCcgNbkgeMDnjq4LWr/PRCzyM5Qa+I/X7XAzydXVxA88d/IMr6l0xfwuTzt/536lqrfIBAEAQBAEAQBAEAQBAEAQAvMYc8HBa0uaSM8gcZHpnqPRQsqV0HW/mRn5WdCvZ71x+tOmZbLVxyC66ObDZ55Q3MM9MWPbRvLvN5ZA5jj59y134xn591rRKi5pPh+/f9zsaDy+/qbDLjwl/xOkFYAgCAIAgCAIAgCAIAgCAIAgCAICjM0OAB8juHzBBH8wFr1fnELPIzk1rb7zWuqy4Zf+sFwkfIOQT9seMZX1Xpf8mjzt/536ltLbIBAFnD9DOH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6BMP0GH6EWgkhrW7zJ901uM5L/CP6rDk61vwRmmovJvP7LFpbS6Vvl6k3d/er+YA/aQyZlJFhpB8wDPIM+rSPJeC67fvvy/kvf+Dr6Dy59/M2nyPUfVefgucnSIq0BAEAQBAEAQBAEAQBAEAQBAEAQFCVzcNORjI5z6kYVFcZRu5RGxNQZya1kf8Apjqzjg6juABHOf8AGSH+x+i+qdLWdGkecv8Azv1LbW5h+hHD9AmH6DD9CGR6j6qtw1Hv7kd7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+oyPUfVHDUe/uN7+pAt7zwNcQ9/hY5vLg48DHxzjCqv8AGqqlOfb/AOkk3N7TqH2VWJ+nNF6ctD42xSx2wVVcx7SyVk9QftDw5p5B3zTjnyY1fOOrS8a2TR29LBxjlmTGjlxx58fBadaxBI3pPKSJ1MiEAQBAEAQBAEAQBAEAQBAEAQEry0NJdjGOc+aZ28mHLYtx8ziwMPGQMEAckkHIWKm5z3NlN12a+DkjqeUS6p1JI+Rru9v9bK0BwP8A4qcf0cPqF9Q0C/gsR7nmbJvx0ePkeo+qucNR7+5Zvf1GR6j6rDjqMvgb39T5O+l/eyfxlbUm/f3NjC9B30v72T+MpJv39xheg76X97J/GUk37+4wvQd9L+9k/jKSb9/cYXoO+l/eyfxlJN+/uML0HfS/vZP4ykm/f3GF6Dvpf3sn8ZSTfv7jC9B30v72T+MpJv39xheg76X97J/GUk37+4wvQd9L+9k/jKSb9/cYXoO+l/eyfxlJN+/uML0Pb0z/AIjUljgqPv4X3WnD4ZvvYn/fM6tPBWl1Rv8ACTXvuydaXiI630TWtpIA1oA7tgwBgYDOAvmWq/Ml7+Z3dP2PSVMPKjIUgEAQBAEAQBAEAQBAEAQBAEAQHzVhIppCCQRjBBwR4goz8rKrvIebM5wpnODnBwHDs8j81LTJO5JmvP8ALOR2oZpjqC4ZllP+9a3rIT/xnL6N01vYjiS/OR5/fS/vZP4yuxJv39y/C9B30v72T+MqQwvQ/9k=",
                      "student": "4fee004cca2e43cf27000005",
                      "id": "4fee004cca2e43cf27000006"
                    },
                    "links": [
                      {
                        "rel": "self",
                        "uri": "/v1.1/students/4fee004cca2e43cf27000005/photo"
                      },
                      {
                        "rel": "student",
                        "uri": "/v1.1/students/4fee004cca2e43cf27000005"
                      }
                    ]
                  }'

  SECTIONS = '{
                "paging": {
                  "current": 1,
                  "total": 15,
                  "count": 73
                },
                "data": [
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "grade": "7",
                      "name": "Advanced Arithmetic 8(A)",
                      "school": "4fee004cca2e43cf27000004",
                      "section_number": "76",
                      "sis_id": "3838",
                      "subject": "math",
                      "teacher": "4fee004dca2e43cf270007f4",
                      "students": [
                        "4fee004dca2e43cf270004ed",
                        "4fee004dca2e43cf2700069f",
                        "4fee004cca2e43cf27000177",
                        "4fee004cca2e43cf270002b7",
                        "4fee004cca2e43cf27000437",
                        "4fee004dca2e43cf2700066b",
                        "4fee004dca2e43cf2700061f",
                        "4fee004dca2e43cf27000701",
                        "4fee004dca2e43cf2700054d",
                        "4fee004cca2e43cf270002b5",
                        "4fee004dca2e43cf27000501",
                        "4fee004dca2e43cf27000617",
                        "4fee004cca2e43cf270000af",
                        "4fee004dca2e43cf27000597",
                        "4fee004dca2e43cf27000719",
                        "4fee004cca2e43cf27000043",
                        "4fee004dca2e43cf27000755",
                        "4fee004cca2e43cf270003df",
                        "4fee004cca2e43cf27000221",
                        "4fee004dca2e43cf2700054b",
                        "4fee004dca2e43cf270004ff",
                        "4fee004cca2e43cf27000325"
                      ],
                      "last_modified": "2012-06-29T19:22:17.766Z",
                      "id": "4fee004dca2e43cf27000807"
                    },
                    "uri": "/v1.1/sections/4fee004dca2e43cf27000807"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "grade": "6",
                      "name": "Planetary Chemistry 5(B)",
                      "school": "4fee004cca2e43cf27000001",
                      "section_number": "85",
                      "sis_id": "4917",
                      "subject": "science",
                      "teacher": "4fee004dca2e43cf270007f9",
                      "students": [
                        "4fee004cca2e43cf2700008b",
                        "4fee004dca2e43cf270006f9",
                        "4fee004cca2e43cf27000201",
                        "4fee004dca2e43cf27000675",
                        "4fee004cca2e43cf2700011f",
                        "4fee004cca2e43cf2700016f",
                        "4fee004dca2e43cf2700076d",
                        "4fee004dca2e43cf27000777",
                        "4fee004cca2e43cf270001b7",
                        "4fee004cca2e43cf270002c9",
                        "4fee004dca2e43cf270005a9",
                        "4fee004cca2e43cf2700030d",
                        "4fee004cca2e43cf2700003f",
                        "4fee004dca2e43cf2700076d",
                        "4fee004dca2e43cf27000769",
                        "4fee004cca2e43cf2700042b",
                        "4fee004dca2e43cf27000551",
                        "4fee004cca2e43cf27000071",
                        "4fee004dca2e43cf270005cd",
                        "4fee004dca2e43cf2700045f",
                        "4fee004dca2e43cf270004bf",
                        "4fee004cca2e43cf27000017",
                        "4fee004dca2e43cf270004f9",
                        "4fee004cca2e43cf27000417",
                        "4fee004cca2e43cf27000213",
                        "4fee004cca2e43cf2700013b",
                        "4fee004dca2e43cf27000699",
                        "4fee004dca2e43cf27000787",
                        "4fee004cca2e43cf27000049",
                        "4fee004dca2e43cf270005e5",
                        "4fee004dca2e43cf2700052d",
                        "4fee004dca2e43cf27000479",
                        "4fee004cca2e43cf2700010f",
                        "4fee004dca2e43cf270004af",
                        "4fee004cca2e43cf27000425"
                      ],
                      "last_modified": "2012-06-29T19:22:17.789Z",
                      "id": "4fee004dca2e43cf27000808"
                    },
                    "uri": "/v1.1/sections/4fee004dca2e43cf27000808"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "grade": "7",
                      "name": "Advanced Literature 2(A)",
                      "school": "4fee004cca2e43cf27000004",
                      "section_number": "90",
                      "sis_id": "3944",
                      "subject": "english/language arts",
                      "teacher": "4fee004dca2e43cf270007d7",
                      "students": [
                        "4fee004dca2e43cf270004a3",
                        "4fee004cca2e43cf27000197",
                        "4fee004dca2e43cf270004b9",
                        "4fee004cca2e43cf270000bd",
                        "4fee004cca2e43cf27000259",
                        "4fee004cca2e43cf27000069",
                        "4fee004dca2e43cf270004c7",
                        "4fee004cca2e43cf270000c1",
                        "4fee004dca2e43cf2700069f",
                        "4fee004dca2e43cf270006bf",
                        "4fee004dca2e43cf27000637",
                        "4fee004dca2e43cf27000733",
                        "4fee004dca2e43cf27000755",
                        "4fee004dca2e43cf2700066b",
                        "4fee004cca2e43cf27000231",
                        "4fee004dca2e43cf270007cb",
                        "4fee004dca2e43cf27000493",
                        "4fee004cca2e43cf270003b3",
                        "4fee004dca2e43cf270006d5",
                        "4fee004cca2e43cf27000211",
                        "4fee004cca2e43cf27000111",
                        "4fee004dca2e43cf27000443"
                      ],
                      "last_modified": "2012-06-29T19:22:17.839Z",
                      "id": "4fee004dca2e43cf2700080a"
                    },
                    "uri": "/v1.1/sections/4fee004dca2e43cf2700080a"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "grade": "8",
                      "name": "Earth Literature 5(B)",
                      "school": "4fee004cca2e43cf27000002",
                      "section_number": "68",
                      "sis_id": "1533",
                      "subject": "english/language arts",
                      "teacher": "4fee004dca2e43cf270007d6",
                      "students": [
                        "4fee004cca2e43cf27000287",
                        "4fee004dca2e43cf27000717",
                        "4fee004cca2e43cf27000173",
                        "4fee004cca2e43cf270000b1",
                        "4fee004cca2e43cf270002f3",
                        "4fee004dca2e43cf2700068b",
                        "4fee004dca2e43cf270005e1",
                        "4fee004dca2e43cf27000579",
                        "4fee004cca2e43cf270000a5",
                        "4fee004cca2e43cf270003e7",
                        "4fee004dca2e43cf2700077f",
                        "4fee004cca2e43cf270003c5",
                        "4fee004cca2e43cf270003a3",
                        "4fee004cca2e43cf270003a1",
                        "4fee004dca2e43cf270006d1",
                        "4fee004cca2e43cf270002f5",
                        "4fee004dca2e43cf27000593",
                        "4fee004dca2e43cf270005c7",
                        "4fee004cca2e43cf2700024b",
                        "4fee004dca2e43cf27000599",
                        "4fee004dca2e43cf2700046d",
                        "4fee004cca2e43cf27000391",
                        "4fee004dca2e43cf27000579",
                        "4fee004cca2e43cf270003a1",
                        "4fee004cca2e43cf270002c7",
                        "4fee004cca2e43cf270000e1",
                        "4fee004dca2e43cf2700070d",
                        "4fee004dca2e43cf27000795"
                      ],
                      "last_modified": "2012-06-29T19:22:17.886Z",
                      "id": "4fee004dca2e43cf2700080c"
                    },
                    "uri": "/v1.1/sections/4fee004dca2e43cf2700080c"
                  },
                  {
                    "data": {
                      "district": "4fd43cc56d11340000000005",
                      "grade": "6",
                      "name": "Earth Biology 4(A)",
                      "school": "4fee004cca2e43cf27000002",
                      "section_number": "57",
                      "sis_id": "2296",
                      "subject": "science",
                      "teacher": "4fee004dca2e43cf270007e7",
                      "students": [
                        "4fee004cca2e43cf27000359",
                        "4fee004cca2e43cf27000039",
                        "4fee004dca2e43cf2700060f",
                        "4fee004cca2e43cf27000391",
                        "4fee004dca2e43cf2700071f",
                        "4fee004cca2e43cf27000247",
                        "4fee004cca2e43cf27000123",
                        "4fee004cca2e43cf270002e5",
                        "4fee004dca2e43cf270006a7",
                        "4fee004cca2e43cf270001f9",
                        "4fee004dca2e43cf27000475",
                        "4fee004dca2e43cf2700068b",
                        "4fee004cca2e43cf27000129",
                        "4fee004cca2e43cf270003e3",
                        "4fee004dca2e43cf270006b7",
                        "4fee004cca2e43cf270003a1",
                        "4fee004dca2e43cf27000749",
                        "4fee004dca2e43cf270006a5",
                        "4fee004cca2e43cf2700034f",
                        "4fee004cca2e43cf270002c7",
                        "4fee004cca2e43cf270000fd",
                        "4fee004cca2e43cf270003d5"
                      ],
                      "last_modified": "2012-06-29T19:22:17.911Z",
                      "id": "4fee004dca2e43cf2700080d"
                    },
                    "uri": "/v1.1/sections/4fee004dca2e43cf2700080d"
                  }
                ],
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/sections?limit=5"
                  },
                  {
                    "rel": "next",
                    "uri": "/v1.1/sections?limit=5&page=2"
                  }
                ]
              }'

  SECTION = '{
                "data": {
                  "district": "4fd43cc56d11340000000005",
                  "grade": "7",
                  "name": "Advanced Arithmetic 8(A)",
                  "school": "4fee004cca2e43cf27000004",
                  "section_number": "76",
                  "sis_id": "3838",
                  "subject": "math",
                  "teacher": "4fee004dca2e43cf270007f4",
                  "students": [
                    "4fee004dca2e43cf270004ed",
                    "4fee004dca2e43cf2700069f",
                    "4fee004cca2e43cf27000177",
                    "4fee004cca2e43cf270002b7",
                    "4fee004cca2e43cf27000437",
                    "4fee004dca2e43cf2700066b",
                    "4fee004dca2e43cf2700061f",
                    "4fee004dca2e43cf27000701",
                    "4fee004dca2e43cf2700054d",
                    "4fee004cca2e43cf270002b5",
                    "4fee004dca2e43cf27000501",
                    "4fee004dca2e43cf27000617",
                    "4fee004cca2e43cf270000af",
                    "4fee004dca2e43cf27000597",
                    "4fee004dca2e43cf27000719",
                    "4fee004cca2e43cf27000043",
                    "4fee004dca2e43cf27000755",
                    "4fee004cca2e43cf270003df",
                    "4fee004cca2e43cf27000221",
                    "4fee004dca2e43cf2700054b",
                    "4fee004dca2e43cf270004ff",
                    "4fee004cca2e43cf27000325"
                  ],
                  "last_modified": "2012-06-29T19:22:17.766Z",
                  "id": "4fee004dca2e43cf27000807"
                },
                "links": [
                  {
                    "rel": "self",
                    "uri": "/v1.1/sections/5009b11c15aec0a77b003086"
                  },
                  {
                    "rel": "district",
                    "uri": "/v1.1/sections/5009b11c15aec0a77b003086/district"
                  },
                  {
                    "rel": "teacher",
                    "uri": "/v1.1/sections/5009b11c15aec0a77b003086/teacher"
                  },
                  {
                    "rel": "students",
                    "uri": "/v1.1/sections/5009b11c15aec0a77b003086/students"
                  },
                  {
                    "rel": "school",
                    "uri": "/v1.1/sections/5009b11c15aec0a77b003086/school"
                  }
                ]
              }'

  SECTION_SCHOOL = '{
                      "data": {
                        "district": "4fd43cc56d11340000000005",
                        "high_grade": "8",
                        "low_grade": "6",
                        "name": "Clever Memorial High",
                        "nces_id": "26309489",
                        "phone": "1-124-215-8079",
                        "school_number": "797",
                        "sis_id": "3935",
                        "location": {
                          "address": "10960 Kilback View",
                          "city": "North Sarina",
                          "state": "MP",
                          "zip": "58717-8256"
                        },
                        "principal": {
                          "name": "Maryse Tillman",
                          "email": "Otto_Sanford@mailinator.com"
                        },
                        "last_modified": "2012-06-29T19:21:50.685Z",
                        "id": "4fee004cca2e43cf27000004"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/schools/4fee004cca2e43cf27000004"
                        }
                      ]
                    }'

  SECTION_DISTRICT = '{
                      "data": {
                        "name": "Demo District",
                        "id": "4fd43cc56d11340000000005"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/districts/4fd43cc56d11340000000005"
                        }
                      ]
                    }'

  SECTION_STUDENTS = '{
                        "paging": {
                          "current": 1,
                          "total": 6,
                          "count": 22
                        },
                        "data": [
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "1/2/1999",
                              "frl_status": "Y",
                              "gender": "M",
                              "grade": "8",
                              "hispanic_ethnicity": "Y",
                              "race": "Caucasian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "8888",
                              "state_id": "1618921",
                              "student_number": "14560",
                              "location": {
                                "address": "91441 Farrell Street",
                                "city": "West Eldridge",
                                "state": "AE",
                                "zip": "43344-2962",
                                "lat": "84.19646366033703",
                                "lon": "159.38663405366242"
                              },
                              "name": {
                                "first": "Mertie",
                                "middle": "H",
                                "last": "Volkman"
                              },
                              "last_modified": "2012-06-29T19:21:51.507Z",
                              "id": "4fee004cca2e43cf27000043"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf27000043"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "2/15/2000",
                              "frl_status": "Y",
                              "gender": "M",
                              "grade": "8",
                              "hispanic_ethnicity": "N",
                              "race": "American Indian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "4631",
                              "state_id": "1758899",
                              "student_number": "17070",
                              "location": {
                                "address": "80754 Russel Crossroad",
                                "city": "Port Nehastad",
                                "state": "VA",
                                "zip": "61938-5279",
                                "lat": "-47.90197472088039",
                                "lon": "153.46314903348684"
                              },
                              "name": {
                                "first": "Lavinia",
                                "middle": "M",
                                "last": "Nader"
                              },
                              "last_modified": "2012-06-29T19:21:52.847Z",
                              "id": "4fee004cca2e43cf270000af"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf270000af"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "4/12/2000",
                              "frl_status": "Y",
                              "gender": "M",
                              "grade": "8",
                              "hispanic_ethnicity": "N",
                              "race": "Asian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "3298",
                              "state_id": "4686624",
                              "student_number": "16107",
                              "location": {
                                "address": "97571 Raynor Junction",
                                "city": "Port Francesco",
                                "state": "UT",
                                "zip": "83548-6038",
                                "lat": "-44.60316952317953",
                                "lon": "-55.60492345131934"
                              },
                              "name": {
                                "first": "Bella",
                                "middle": "C",
                                "last": "Keeling"
                              },
                              "last_modified": "2012-06-29T19:21:55.361Z",
                              "id": "4fee004cca2e43cf27000177"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf27000177"
                          },
                          {
                            "data": {
                              "district": "4fd43cc56d11340000000005",
                              "dob": "2/7/2000",
                              "frl_status": "N",
                              "gender": "F",
                              "grade": "6",
                              "hispanic_ethnicity": "N",
                              "race": "American Indian",
                              "school": "4fee004cca2e43cf27000004",
                              "sis_id": "6202",
                              "state_id": "2990051",
                              "student_number": "33343",
                              "location": {
                                "address": "8077 Towne Throughway",
                                "city": "North Lillieburgh",
                                "state": "VT",
                                "zip": "90260",
                                "lat": "-86.87210537027568",
                                "lon": "-73.90811132267118"
                              },
                              "name": {
                                "first": "Lauryn",
                                "middle": "B",
                                "last": "Harvey"
                              },
                              "last_modified": "2012-06-29T19:21:57.524Z",
                              "id": "4fee004cca2e43cf27000221"
                            },
                            "uri": "/v1.1/students/4fee004cca2e43cf27000221"
                          }
                        ],
                        "links": [
                          {
                            "rel": "self",
                            "uri": "/v1.1/sections/4fee004dca2e43cf27000807/students?limit=4"
                          },
                          {
                            "rel": "next",
                            "uri": "/v1.1/sections/4fee004dca2e43cf27000807/students?limit=4&page=2"
                          }
                        ]
                      }'

  SECTION_TEACHER = '{
                      "data": {
                        "district": "4fd43cc56d11340000000005",
                        "email": "jermaine.lebsack@mailinator.com",
                        "school": "4fee004cca2e43cf27000004",
                        "sis_id": "8064",
                        "title": "Strategize Next-generation Solutions Manager",
                        "name": {
                          "first": "Estevan",
                          "middle": "N",
                          "last": "Braun"
                        },
                        "last_modified": "2012-06-29T19:22:17.301Z",
                        "id": "4fee004dca2e43cf270007f4"
                      },
                      "links": [
                        {
                          "rel": "self",
                          "uri": "/v1.1/teachers/4fee004dca2e43cf270007f4"
                        }
                      ]
                    }'
end