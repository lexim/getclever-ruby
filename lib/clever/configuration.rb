module Clever
  class Configuration
    include ActiveSupport::Configurable
    config_accessor :api_key

    def param_name
      config.param_name.respond_to?(:call) ? config.param_name.call : config.param_name
    end
  end
end