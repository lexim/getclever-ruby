module Clever
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      class_option :api_key, :type => :string, :banner => 'Your Clever API key', :required => true

      def create_config_file
        template 'clever.yml', File.join('config', 'clever.yml')
      end

      def create_initializer
        template 'initializer.rb', File.join('config', 'initializers', 'clever.rb')
      end

      desc <<DESC
Description:
    Copies Clever configuration file to your application's initializer directory.
DESC

      def self.source_root
        @source_root ||= File.expand_path(File.join(File.dirname(__FILE__), 'templates'))
      end
    end
  end
end
