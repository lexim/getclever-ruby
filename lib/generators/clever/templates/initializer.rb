Clever.configure do |config|
  config.api_key = YAML.load_file(Rails.root.join("config/clever.yml"))[Rails.env]['token']
end
