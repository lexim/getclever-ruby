require 'active_support/configurable'
require 'clever/configuration'

module Clever
  class << self
    def configure(&block)
      yield @config ||= Configuration.new
    end

    def config
      @config
    end
  end
  module Models end
  module Endpoints end
end