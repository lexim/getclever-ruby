# -*- encoding: utf-8 -*-
require File.expand_path('../lib/clever/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = %w(Lexim)
  gem.email         = %w(hello@getlexim.com)
  gem.summary       = %q{Ruby library for interacting with the Clever API}
  gem.homepage      = "http://github.com/lexim/getclever-ruby"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "getclever-ruby"
  gem.require_paths = ["lib"]
  gem.version       = Clever::VERSION

  gem.add_dependency 'json'
  gem.add_dependency 'activesupport'

  gem.add_development_dependency "rspec"
  gem.add_development_dependency "spork", '~> 0.9.0.rc'
  gem.add_development_dependency 'guard-rspec', '~> 0.7.0'
  gem.add_development_dependency 'terminal-notifier-guard'
end
