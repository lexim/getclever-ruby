require 'spec_helper'

describe 'configuration' do
  it 'should accept an api_key and save it' do
    Clever.configure do |config|
      config.api_key = 'test1234'
    end

    Clever.config.api_key.should eq('test1234')
  end
end