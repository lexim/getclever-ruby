require 'spec_helper'
require 'clever/models/photo'

describe Clever::Models::Photo do
  it 'requires initialization parameters' do
    expect {Clever::Models::Photo.new}.to raise_error(ArgumentError)
  end

  context "valid values" do
    id = '4fee004cca2e43cf27000001'
    data = 'lots of bytes'

    let(:params) {{
        :id => id,
        :data => data
    }}

    subject { Clever::Models::Photo.new params }

    its(:id)              { should == id }
    its(:data)            { should == data }
  end
end