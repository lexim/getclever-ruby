require 'spec_helper'
require 'clever/models/contact'

describe Clever::Models::Contact do
  it 'requires initialization parameters' do
    expect {Clever::Models::Contact.new}.to raise_error(ArgumentError)
  end

  context "valid values" do
    id = '4fee004dca2e43cf27000931'
    name = 'Issac Roob'
    email = 'Samanta.Schaefer@mailinator.com'
    phone = '597.747.8259'
    phone_type = 'home'
    type = 'guardian'
    student_id = '4fee004dca2e43cf27000931'

    let(:params) {{
        :id => id,
        :name => name,
        :email => email,
        :phone => phone,
        :phone_type => phone_type,
        :type => type,
        :student_id => student_id
    }}

    subject { Clever::Models::Contact.new params }

    its(:id)              { should == id }
    its(:name)            { should == name }
    its(:email)           { should == email }
    its(:phone)           { should == phone }
    its(:phone_type)      { should == phone_type }
    its(:type)            { should == type }
    its(:student_id)      { should == student_id }
  end
end