require 'spec_helper'
require 'clever/models/section'

describe Clever::Models::Section do
  it 'requires initialization parameters' do
    expect {Clever::Models::Section.new}.to raise_error(ArgumentError)
  end

  context "valid values" do
    id = '4fee004dca2e43cf27000807'
    district_id = '4fd43cc56d11340000000005'
    name = 'Advanced Arithmetic 8(A)'
    grade = '7'
    school_id = '4fee004cca2e43cf27000003'
    section_number = '73'
    sis_id = '1783'
    subject = 'math'
    teacher_id = '4fee004dca2e43cf270007f4'
    last_modified = '2012-06-29T19:22:17.766Z'
    students = %w(4fee004dca2e43cf2700069f 4fee004dca2e43cf270004ed)

    let(:params) {{
        :id => id,
        :name => name,
        :sis_id => sis_id,
        :last_modified => last_modified,
        :district_id => district_id,
        :school_id => school_id,
        :section_number => section_number,
        :subject => subject,
        :teacher_id => teacher_id,
        :students => students
    }}

    subject { Clever::Models::Section.new params }

    its(:id)                      { should == id }
    its(:name)                    { should == name }
    its(:sis_id)                  { should == sis_id }
    its(:last_modified)           { should == last_modified }
    its(:district_id)             { should == district_id }
    its(:school_id)               { should == school_id }
    its(:section_number)          { should == section_number }
    its(:subject)                 { should == subject }
    its(:teacher_id)              { should == teacher_id }
    its(:students)                { should == students }
  end
end
