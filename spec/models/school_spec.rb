require 'spec_helper'
require 'clever/models/school'

describe Clever::Models::School do
  it 'requires initialization parameters' do
    expect {Clever::Models::School.new}.to raise_error(ArgumentError)
  end

  context "valid values" do
    id = '4fee004cca2e43cf27000001'
    name = 'Clever Academy'
    high_grade = '8'
    low_grade = '6'
    nces_id = '17065379'
    phone = '1-755-019-5442'
    school_number = '214'
    sis_id = '9255'
    location = { :address => "18828 Kutch Court", :city => "Dessiemouth", :state => "IA", :zip => "37471-9969" }
    principal = { :name => "Colleen Gottlieb", :email => "Eda_Barrows@mailinator.com" }
    last_modified = '2012-06-29T19:21:50.592Z'
    district_id = '4fd43cc56d11340000000005'

    let(:params) {{
        :id => id,
        :name => name,
        :high_grade => high_grade,
        :low_grade => low_grade,
        :nces_id => nces_id,
        :phone => phone,
        :school_number => school_number,
        :sis_id => sis_id,
        :location => location,
        :principal => principal,
        :last_modified => last_modified,
        :district_id => district_id
    }}

    subject { Clever::Models::School.new params }

    its(:id)              { should == id }
    its(:name)            { should == name }
    its(:high_grade)      { should == high_grade }
    its(:low_grade)       { should == low_grade }
    its(:nces_id)         { should == nces_id }
    its(:phone)           { should == phone }
    its(:school_number)   { should == school_number }
    its(:sis_id)          { should == sis_id }
    its(:location)        { should == location }
    its(:principal)       { should == principal }
    its(:last_modified)   { should == last_modified }

  end
end