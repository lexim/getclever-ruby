require 'spec_helper'
require 'clever/models/teacher'

describe Clever::Models::Teacher do
  it 'requires initialization parameters' do
    expect { Clever::Models::Teacher.new }.to raise_error(ArgumentError)
  end

  context "valid values" do
    district_id = '4fd43cc56d11340000000005',
    email = 'ashlee@mailinator.com',
    school_id = '4fee004cca2e43cf27000003',
    sis_id = '1720',
    title = 'Orchestrate Visionary Technologies Manager',
    name = {
        :first => 'Nadia',
        :middle => 'H',
        :last => 'Mitchell'
    },
    last_modified = '2012-06-29T19:22:16.552Z',
    id = '4fee004dca2e43cf270007d5'

    let(:params) {{
        :id => id,
        :name => name,
        :email => email,
        :title => title,
        :sis_id => sis_id,
        :last_modified => last_modified,
        :school_id => school_id,
        :district_id => district_id
    }}

    subject { Clever::Models::Teacher.new params }

    its(:id)                  { should == id }
    its(:name)                { should == name }
    its(:email)               { should == email }
    its(:title)               { should == title }
    its(:sis_id)              { should == sis_id }
    its(:last_modified)       { should == last_modified }
    its(:school_id)           { should == school_id }
    its(:district_id)         { should == district_id }

  end
end
