require 'spec_helper'
require 'clever/models/district'

describe Clever::Models::District do
  it 'requires an id and name' do
    expect { Clever::Models::District.new }.to raise_error(ArgumentError)
  end

  context "valid values" do

    id = '4fd43cc56d11340000000005'
    name = 'Test District'

    let(:params) { {
        :id => id,
        :name => name
    } }

    subject { Clever::Models::District.new params }

    its(:id) { should == id }
    its(:name) { should == name }
  end
end