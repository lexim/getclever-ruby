require 'spec_helper'
require 'clever/models/student'

describe Clever::Models::Student do
  it 'requires initialization parameters' do
    expect {Clever::Models::Student.new}.to raise_error(ArgumentError)
  end

  context "valid values" do
    id = '4fee004cca2e43cf27000005'
    district_id = '4fd43cc56d11340000000005'
    dob = '12/12/1998'
    frl_status = 'Y'
    gender = 'M'
    grade = '7'
    hispanic_ethnicity = 'N'
    race = 'Asian'
    school_id = '4fee004cca2e43cf27000003'
    sis_id = '1783'
    state_id = '2237504'
    student_number = '24772'
    location = {
        :address => '9272 Ratke Haven',
        :city => 'North Maurinetown',
        :state => 'AP',
        :zip => '22505-3577',
        :lat => '-21.901449509896338',
        :lon => '114.23126022331417'
    }
    name = {
        :first => 'Constance',
        :middle => '',
        :last => 'Roob'
    }
    last_modified = '2012-06-29T19:21:50.709Z'

    let(:params) {{
        :id => id,
        :name => name,
        :sis_id => sis_id,
        :location => location,
        :last_modified => last_modified,
        :district_id => district_id,
        :school_id => school_id,
        :state_id => state_id,
        :dob => dob,
        :frl_status => frl_status,
        :gender => gender,
        :grade => grade,
        :hispanic_ethnicity => hispanic_ethnicity,
        :race => race,
        :student_number => student_number
    }}

    subject { Clever::Models::Student.new params }

    its(:id)                      { should == id }
    its(:name)                    { should == name }
    its(:sis_id)                  { should == sis_id }
    its(:location)                { should == location }
    its(:last_modified)           { should == last_modified }
    its(:district_id)             { should == district_id }
    its(:school_id)               { should == school_id }
    its(:state_id)                { should == state_id }
    its(:dob)                     { should == dob }
    its(:frl_status)              { should == frl_status }
    its(:gender)                  { should == gender }
    its(:grade)                   { should == grade }
    its(:hispanic_ethnicity)      { should == hispanic_ethnicity }
    its(:race)                    { should == race }
    its(:student_number)          { should == student_number }
  end
end
