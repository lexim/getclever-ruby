require 'spec_helper'
require 'clever/api'

describe 'API' do
  before do
    Clever.configure do |config|
      config.api_key = 'DEMO_KEY'
    end

    @api = Clever::API.new
    @api.http = Clever::FakeServer.new({:api_key => Clever.config.api_key})
  end

  it "can get a list of districts" do
    districts = @api.districts

    districts.should.respond_to? :data

    # exclude paging tests if no paging data found (accommodate live server tests rather than stubbed tests)
    unless districts[:paging].nil?
      districts[:paging][:current].should eq(1)
      districts[:paging][:total].should eq(1)
      districts[:paging][:count].should eq(1)
    end

    districts[:data][0].id.should eq('4fd43cc56d11340000000005')
    districts[:data][0].name.should eq('Demo District')
  end

  it "can get a specific district" do
    district = @api.district('4fd43cc56d11340000000005')

    district.name.should eq('Demo District')
  end

  it "can get a list of schools for a given district" do
    schools = @api.district_schools('4fd43cc56d11340000000005')

    schools[:data].length.should eq(4)
    schools[:data][0].name.should eq('Clever Academy')
  end

  it "can get a list of teachers for a given district" do
    teachers = @api.district_teachers('4fd43cc56d11340000000005', {:limit => 10})

    # exclude paging tests if no paging data found (accommodate live server tests rather than stubbed tests)
    unless teachers[:paging].nil?
      teachers[:paging][:current].should eq(1)
      teachers[:paging][:total].should eq(5)
      teachers[:paging][:count].should eq(50)
    end

    teachers[:data].length.should eq(10)

    teachers[:data][0].id.should eq('4fee004dca2e43cf270007d5')
    teachers[:data][0].email.should eq('ashlee@mailinator.com')
  end

  it "can get a list of students for a given district" do
    students = @api.district_students('4fd43cc56d11340000000005', {:limit => 5})

    unless students[:paging].nil?
      students[:paging][:current].should eq(1)
      students[:paging][:total].should eq(200)
      students[:paging][:count].should eq(1000)
    end

    students[:data].length.should eq(5)
    students[:data][0].id.should eq('4fee004cca2e43cf27000005')
    students[:data][0].name['first'].should eq('Constance')
  end

  it "can get a list of sections for a given district" do
    sections = @api.district_sections('4fd43cc56d11340000000005', {:limit => 2})

    unless sections[:paging].nil?
      sections[:paging][:current].should eq(1)
      sections[:paging][:total].should eq(37)
      sections[:paging][:count].should eq(73)
    end

    sections[:data].length.should eq(2)
    sections[:data][0].id.should eq('4fee004dca2e43cf27000807')
    sections[:data][0].name.should eq('Advanced Arithmetic 8(A)')
  end
end
