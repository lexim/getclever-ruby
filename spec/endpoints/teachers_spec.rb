require 'spec_helper'
require 'clever/api'

describe 'API' do
  before do
    Clever.configure do |config|
      config.api_key = 'DEMO_KEY'
    end

    @api = Clever::API.new
    @api.http = Clever::FakeServer.new({:api_key => Clever.config.api_key})
  end

  it "can get a list of teachers" do
    teachers = @api.teachers({:limit => 4})

    teachers[:data].length.should eq(4)

    teachers[:data][0].id.should eq('4fee004dca2e43cf270007d5')
    teachers[:data][0].name['first'].should eq('Nadia')
    teachers[:data][0].name['middle'].should eq('H')
    teachers[:data][0].name['last'].should eq('Mitchell')
  end

  it "can get a specific teacher" do
    teacher = @api.teacher('4fee004dca2e43cf270007d5')

    teacher.id.should eq('4fee004dca2e43cf270007d5')
    teacher.name['first'].should eq('Nadia')
    teacher.name['middle'].should eq('H')
    teacher.name['last'].should eq('Mitchell')
  end

  it "can get a list of sections for a given teacher" do
    sections = @api.teacher_sections('4fee004dca2e43cf270007fd', {:limit => 2})

    unless sections[:paging].nil?
      sections[:paging][:current].should eq(1)
      sections[:paging][:total].should eq(2)
      sections[:paging][:count].should eq(3)
    end

    sections[:data].length.should eq(2)

    sections[:data][0].id.should eq('4fee004dca2e43cf2700081a')
    sections[:data][0].name.should eq('Earth Arithmetic 4(B)')
  end

  it "can get a school for a given teacher" do
    school = @api.teacher_school('4fee004dca2e43cf270007d5')

    school.id.should eq('4fee004cca2e43cf27000003')
    school.name.should eq('Clever High School')
  end

  it "can get a district for a given teacher" do
    district = @api.teacher_district('4fee004dca2e43cf270007fd')

    district.id.should eq('4fd43cc56d11340000000005')
    district.name.should eq('Demo District')
  end

  it "can get a list of students for a given teacher" do
    students = @api.teacher_students('4fee004dca2e43cf270007fd', {:limit => 5})

    unless students[:paging].nil?
      students[:paging][:current].should eq(1)
      students[:paging][:total].should eq(17)
      students[:paging][:count].should eq(81)
    end

    students[:data].length.should eq(5)
    students[:data][0].id.should eq('4fee004cca2e43cf2700000d')
  end

  it "can get a list of grade levels for a given teacher" do
    grade_levels = @api.teacher_grade_levels('4fee004dca2e43cf270007fd')

    grade_levels.length.should eq(3)

    grade_levels[0].should eq(6)
    grade_levels[1].should eq(7)
    grade_levels[2].should eq(8)
  end
end