require 'spec_helper'
require 'clever/api'

describe 'API' do
  before do
    Clever.configure do |config|
      config.api_key = 'DEMO_KEY'
    end

    @api = Clever::API.new
    @api.http = Clever::FakeServer.new({:api_key => Clever.config.api_key})
  end

  it "can get a list of sections" do
    sections = @api.sections

    sections.should.respond_to? :data

    # exclude paging tests if no paging data found (accommodate live server tests rather than stubbed tests)
    unless sections[:paging].nil?
      sections[:paging][:current].should eq(1)
      sections[:paging][:total].should eq(15)
      sections[:paging][:count].should eq(73)
    end

    sections[:data][0].id.should eq('4fee004dca2e43cf27000807')
    sections[:data][0].name.should eq('Advanced Arithmetic 8(A)')
  end

  it "can get a specific section" do
    section = @api.section('4fee004dca2e43cf27000807')

    section.id.should eq('4fee004dca2e43cf27000807')
    section.name.should eq('Advanced Arithmetic 8(A)')
  end

  it "can get a school for a given section" do
    school = @api.section_school('4fee004dca2e43cf27000807')

    school.name.should eq('Clever Memorial High')
  end

  it "can get a district for a given section" do
    district = @api.section_district('4fee004dca2e43cf27000807')

    district.name.should eq('Demo District')
    district.id.should eq('4fd43cc56d11340000000005')
  end

  it "can get a list of students for a given section" do
    students = @api.section_students('4fee004dca2e43cf27000807', {:limit => 4})

    unless students[:paging].nil?
      students[:paging][:current].should eq(1)
      students[:paging][:total].should eq(6)
      students[:paging][:count].should eq(22)
    end

    students[:data].length.should eq(4)
    students[:data][0].id.should eq('4fee004cca2e43cf27000043')
    students[:data][0].name['first'].should eq('Mertie')
    students[:data][0].name['m'].should eq(nil)
    students[:data][0].name['last'].should eq('Volkman')
  end

  it "can get a teacher for a given section" do
    teacher = @api.section_teacher('4fee004dca2e43cf27000807')

    teacher.id.should eq('4fee004dca2e43cf270007f4')
    teacher.name['first'].should eq('Estevan')
    teacher.name['middle'].should eq('N')
    teacher.name['last'].should eq('Braun')
  end
end