require 'spec_helper'
require 'clever/api'

describe 'API' do
  before do
    Clever.configure do |config|
      config.api_key = 'DEMO_KEY'
    end

    @api = Clever::API.new
    @api.http = Clever::FakeServer.new({:api_key => Clever.config.api_key})
  end

  it "can get a list of schools" do
    schools = @api.schools({:limit => 2})

    schools.should.respond_to?(:data)

    # exclude paging tests if no paging data found (accommodate live server tests rather than stubbed tests)
    unless schools[:paging].nil?
      schools[:paging][:current].should eq(1)
      schools[:paging][:total].should eq(2)
      schools[:paging][:count].should eq(4)
    end

    schools[:data].length.should eq(2)

    schools[:data][0].id.should eq('4fee004cca2e43cf27000001')
    schools[:data][0].name.should eq('Clever Academy')
  end

  it "can get a specific school" do
    school = @api.school('4fee004cca2e43cf27000001')

    school.name.should eq('Clever Academy')
  end

  it "can get a list of teachers for a given school" do
    teachers = @api.school_teachers('4fee004cca2e43cf27000001', {:limit => 10})

    # exclude paging tests if no paging data found (accommodate live server tests rather than stubbed tests)
    unless teachers[:paging].nil?
      teachers[:paging][:current].should eq(1)
      teachers[:paging][:total].should eq(2)
      teachers[:paging][:count].should eq(12)
    end

    teachers[:data].length.should eq(10)

    teachers[:data][0].id.should eq('4fee004dca2e43cf270007d8')
    teachers[:data][0].email.should eq('adaline_kunze@mailinator.com')
  end

  it "can get a list of students for a given school" do
    students = @api.school_students('4fee004cca2e43cf27000001', {:limit => 4})

    unless students[:paging].nil?
      students[:paging][:current].should eq(1)
      students[:paging][:total].should eq(60)
      students[:paging][:count].should eq(239)
    end

    students[:data].length.should eq(4)
    students[:data][0].id.should eq('4fee004cca2e43cf27000013')
    students[:data][0].name['first'].should eq('Jacinthe')
  end

  it "can get a list of sections for a given school" do
    sections = @api.school_sections('4fee004cca2e43cf27000001', {:limit => 2})

    unless sections[:paging].nil?
      sections[:paging][:current].should eq(1)
      sections[:paging][:total].should eq(9)
      sections[:paging][:count].should eq(17)
    end

    sections[:data].length.should eq(2)
    sections[:data][0].id.should eq('4fee004dca2e43cf27000808')
    sections[:data][0].name.should eq('Planetary Chemistry 5(B)')
  end

  it "can get the school's district" do
    district = @api.school_district('4fee004cca2e43cf27000001')

    district.name.should eq('Demo District')
    district.id.should eq('4fd43cc56d11340000000005')
  end
end