require 'spec_helper'
require 'clever/api'

describe 'API' do
  before do
    Clever.configure do |config|
      config.api_key = 'DEMO_KEY'
    end

    @api = Clever::API.new
    @api.http = Clever::FakeServer.new({:api_key => Clever.config.api_key})
  end

  it "can get a list of students" do
    students = @api.students({:limit => 5})

    unless students[:paging].nil?
      students[:paging][:current].should eq(1)
      students[:paging][:total].should eq(200)
      students[:paging][:count].should eq(1000)
    end

    students[:data].length.should be(5)

    students[:data][0].id.should eq('4fee004cca2e43cf27000005')
    students[:data][0].name['first'].should eq('Constance')
    students[:data][0].name['middle'].should eq('')
    students[:data][0].name['last'].should eq('Roob')
  end

  it "can get a specific student" do
    student = @api.student('4fee004cca2e43cf27000005')

    student.id.should eq('4fee004cca2e43cf27000005')
    student.name['first'].should eq('Constance')
    student.name['middle'].should eq('')
    student.name['last'].should eq('Roob')
  end

  it "can get a list of sections for a given student" do
    sections = @api.student_sections('4fee004cca2e43cf27000005', {:limit => 2})

    unless sections[:paging].nil?
      sections[:paging][:current].should eq(1)
      sections[:paging][:total].should eq(2)
      sections[:paging][:count].should eq(4)
    end

    sections[:data].length.should eq(2)

    sections[:data][0].id.should eq('4fee004dca2e43cf27000818')
    sections[:data][0].name.should eq('Planetary Chemistry 1(B)')
  end

  it "can get a school for a given student" do
    school = @api.student_school('4fee004cca2e43cf27000005')

    school.id.should eq('4fee004cca2e43cf27000003')
    school.name.should eq('Clever High School')
  end

  it "can get a district for a given student" do
    district = @api.student_district('4fee004cca2e43cf27000005')

    district.id.should eq('4fd43cc56d11340000000005')
    district.name.should eq('Demo District')
  end

  it "can get a list of teachers for a given student" do
    teachers = @api.student_teachers('4fee004cca2e43cf27000005', {:limit => 2})

    unless teachers[:paging].nil?
      teachers[:paging][:current].should eq(1)
      teachers[:paging][:total].should eq(2)
      teachers[:paging][:count].should eq(4)
    end

    teachers[:data].length.should eq(2)

    teachers[:data][0].id.should eq('4fee004dca2e43cf270007d9')
    teachers[:data][0].name['first'].should eq('Bridget')
    teachers[:data][0].name['middle'].should eq('B')
    teachers[:data][0].name['last'].should eq('Kulas')
  end

  it "can get a list of contacts for a given student" do
    contacts = @api.student_contacts('4fee004cca2e43cf27000005', {:limit => 2})

    contacts[:data].length.should eq(6)
    contacts[:data][0].email.should eq('Samanta.Schaefer@mailinator.com')
    contacts[:data][0].type.should eq('guardian')

  end

  it "can get a photo for a given student" do
    photo = @api.student_photo('4fee004cca2e43cf27000005')

    photo.id.should eq('4fee004cca2e43cf27000006')
  end
end
