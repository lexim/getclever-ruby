require 'rubygems'
require 'spork'

Spork.prefork do

end

Spork.each_run do

end

lib_dir = File.expand_path(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

require 'clever'
require 'clever/fake_server'